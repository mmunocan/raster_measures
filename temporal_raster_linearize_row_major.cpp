/*
 * Given a temporal raster, linearize it using row-major Curve
 */

#include <iostream>
#include <chrono>
#include <fstream>
#include <cassert>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 4){
		cerr << "ERROR! USE " << argv[0] << " <inputs_filename> <inputs_path_folder> <output_filename>" << endl;
		return -1;
	}
	
	string inputs_filename = argv[1];
	string inputs_path_folder = argv[2];
	string output_filename = argv[3];
	
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	size_t n_cells = n_rows*n_cols;
	
	ofstream output_file(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	
	string raster_input_filename;
	int * raster_values = new int[n_cells];
	while(inputs_file >> raster_input_filename){
		raster_input_filename = inputs_path_folder + "/" + raster_input_filename;
		ifstream raster_input_file(raster_input_filename, ios::binary);
		assert(raster_input_file.is_open() && raster_input_file.good());
		
		raster_input_file.read((char*)raster_values, n_cells * sizeof(int));
		output_file.write((char*)raster_values, n_cells * sizeof(int));
		
		raster_input_file.close();
	}
	
	inputs_file.close();
	output_file.close();
	
	return 0;
}