#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3

mkdir -p $output_path
mkdir -p "$output_path/rm_context"
mkdir -p "$output_path/rm_variance_context"
mkdir -p "$output_path/z_context"
mkdir -p "$output_path/z_variance_context"
mkdir -p "$output_path/h_context"
mkdir -p "$output_path/h_variance_context"
mkdir -p "$output_path/rd_context"
mkdir -p "$output_path/rd_variance_context"

i=0
while IFS= read -r line
do
	set -- $line
		filename=$1
		n_rows=$2
		n_cols=$3
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		
		# measures 1d
		nohup ./compute_context "$origin_path/rm_$filename" > "$output_path/rm_context/$output_result" &
		nohup ./compute_context_variance "$origin_path/rm_$filename" > "$output_path/rm_variance_context/$output_result" &
		nohup ./compute_context "$origin_path/z_$filename" > "$output_path/z_context/$output_result" &
		nohup ./compute_context_variance "$origin_path/z_$filename" > "$output_path/z_variance_context/$output_result" &
		nohup ./compute_context "$origin_path/h_$filename" > "$output_path/h_context/$output_result" &
		nohup ./compute_context_variance "$origin_path/h_$filename" > "$output_path/h_variance_context/$output_result" &
		nohup ./compute_context "$origin_path/rd_$filename" > "$output_path/rd_context/$output_result" &
		nohup ./compute_context_variance "$origin_path/rd_$filename" > "$output_path/rd_variance_context/$output_result" &
		
		i=$(($i + 1))
done < $list_files