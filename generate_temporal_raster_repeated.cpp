/*
 * Given a temporal raster, generate a new temporal raster where all rasters 
 * are the same first raster of the input temporal raster
 */

#include <iostream>
#include <chrono>
#include <fstream>
#include <cassert>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 7){
		cerr << "ERROR! USE " << argv[0] << " <inputs_filename> <inputs_path_folder> <outputs_filename> <outputs_path_folder> <raster_initial_filename> <number_of_rasters>" << endl;
		return -1;
	}
	
	string inputs_filename = argv[1];
	string inputs_path_folder = argv[2];
	string outputs_filename = argv[3];
	string outputs_path_folder = argv[4];
	string raster_initial_filename = argv[5];
	int number_of_rasters = atoi(argv[6]);
	
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	size_t n_cells = n_rows*n_cols;
	
	string raster_input_filename;
	int * raster_values = new int[n_cells];
	inputs_file >> raster_input_filename;
	
	raster_input_filename = inputs_path_folder + "/" + raster_input_filename;
	ifstream raster_input_file(raster_input_filename, ios::binary);
	assert(raster_input_file.is_open() && raster_input_file.good());

	raster_input_file.read((char*)raster_values, n_cells * sizeof(int));
	
	raster_input_file.close();
	inputs_file.close();
	
	ofstream outputs_file(outputs_filename);
	assert(outputs_file.is_open() && outputs_file.good());
	
	outputs_file << n_rows << " " << n_cols << " " << k1 << " " << k2 << " " << level_k1 << " " << plain_levels << endl;
	string raster_output_filename;
	for(int i = 0; i < number_of_rasters; i++){
		raster_output_filename = raster_initial_filename + "_" + to_string(i) + ".bin";
		outputs_file << raster_output_filename << endl;
		
		raster_output_filename = outputs_path_folder + "/" + raster_output_filename;
		ofstream raster_output_file(raster_output_filename, ios::binary);
		assert(raster_output_file.is_open() && raster_output_file.good());
		
		raster_output_file.write((char*)raster_values, n_cells * sizeof(int));
		
		raster_output_file.close();
	}
	
	outputs_file.close();
	
	return 0;
}