#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3


i=0
while IFS= read -r line
do
	set -- $line
		filename=$1
		n_rows=$2
		n_cols=$3
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		# linearizacion
		./continuous_alphabet "$origin_path/$filename" "$output_path/rm_$filename"
		./linearize_zorder "$output_path/rm_$filename" $n_rows $n_cols "$output_path/z_$filename"
		./linearize_hilbert "$output_path/rm_$filename" $n_rows $n_cols "$output_path/h_$filename"
		./linearize_isuffix "$output_path/rm_$filename" $n_rows $n_cols "$output_path/is_$filename"
		
		i=$(($i + 1))
done < $list_files