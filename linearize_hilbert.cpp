/*
 * Given a raster, linearize it using Hilbert Curve
 */

#include <iostream>
#include <map>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cassert>

using namespace std;

unsigned int get_side(unsigned int rows, unsigned int cols){
	if(rows >= cols){
		return  (1 << (32-__builtin_clz(rows-1)));
	}else{
		return  (1 << (32-__builtin_clz(cols-1)));
	}
}

/* 
 * Source of rot() and xy2d(): https://en.wikipedia.org/wiki/Hilbert_curve
 */
//rotate/flip a quadrant appropriately
void rot(int n, int *x, int *y, int rx, int ry) {
	if (ry == 0) {
		if (rx == 1) {
			*x = n-1 - *x;
			*y = n-1 - *y;
		}

		//Swap x and y
		int t  = *x;
		*x = *y;
		*y = t;
	}
}

int xy2d(int n, int x, int y) {
	int rx, ry, s, d=0;
	for (s=n/2; s>0; s/=2) {
		rx = (x & s) > 0;
		ry = (y & s) > 0;
		d += s * s * ((3 * rx) ^ ry);
		rot(n, &x, &y, rx, ry);
	}
	return d;
}


void linearize_hilbert(const string & input_filename, size_t n_rows, size_t n_cols, const string & output_filename){
	// Read the input
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	size_t total_cells = n_rows * n_cols;
	unsigned int * data = new unsigned int[total_cells];
	
	input_file.read((char*)data, total_cells * sizeof(unsigned int));
	input_file.close();
	
	// Linearizate in hilbert
	unsigned int side = get_side(n_rows, n_cols);
	unsigned int double_side = side * side;
	vector<unsigned int> hilbert(double_side, 0);
	vector<bool> is_ready(double_side, false);
	
	size_t index = 0;
	size_t current_cell = 0;
	for(size_t r = 0; r < n_rows; r++){
		for(size_t c = 0; c < n_cols; c++){
			index = xy2d(double_side, c, r);
			hilbert[index] = data[current_cell];
			is_ready[index] = true;
			current_cell++;
		}
	}
	
	unsigned int * new_hilbert = new unsigned int[total_cells];
	index = 0;
	for(size_t i = 0; i < double_side; i++){
		if(is_ready[i]){
			new_hilbert[index] = hilbert[i];
			index++;
		}
	}
	
	// Write the output
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)new_hilbert, total_cells * sizeof(unsigned int));
	output_file.close();
	
}

int main(int argc, char ** argv){
	
	if(argc != 5){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <n_rows> <n_cols> <output_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	unsigned int n_rows = atoi(argv[2]);
	unsigned int n_cols = atoi(argv[3]);
	string output_filename = argv[4];
	
	linearize_hilbert(input_filename, n_rows, n_cols, output_filename);
	
	cout << "File " << output_filename << " from " << input_filename << " ready! " << endl;
	
	return 0;
}