/* 
 * Given a text, compute the variance of the contexts' frequencies
 */
#include <iostream>
#include <fstream>
#include <cassert>
#include <iomanip>
#include <map>
#include <cmath>

using namespace std;

void compute_variance(const string & filename){
	// Read sequence
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int *data = new int[n];
	
	input_file.read((char*)data, n*sizeof(int));
	input_file.close();
	
	// Compute context
	cout << filename << ";";
	cout << fixed << setprecision(20);
	map<string, size_t> kmers;
	string kmer;
	double mean, variance;
	size_t s, e;
	for(size_t k = 1; k < 9; k++){
		kmers.clear();
		for(size_t i = 0; i < n-k+1; i++){
			s = i*sizeof(int);
			e = (i+k)*sizeof(int);
			kmer.assign((char*)data+s, (char*)data+e);
			if(!kmers.count(kmer)){
				kmers[kmer] = 1;
			}else{
				kmers[kmer]++;
			}
		}
		
		variance = 0;
		mean = (n-k+1) / kmers.size();
		
		for(const auto &kmer_t : kmers) {
			variance += ((kmer_t.second - mean) * (kmer_t.second - mean));
		}
		
		variance /= n-k;
		
		cout << variance << ";";
		
	}
	cout << endl;
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	compute_variance(filename);
	
	return 0;
}