#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3

mkdir -p $output_path
mkdir -p "$output_path/rm_hk"
mkdir -p "$output_path/rm_runs"
mkdir -p "$output_path/rm_z"
mkdir -p "$output_path/rm_v"
mkdir -p "$output_path/rm_g"
mkdir -p "$output_path/rm_r"
mkdir -p "$output_path/rm_delta"
mkdir -p "$output_path/z_hk"
mkdir -p "$output_path/z_runs"
mkdir -p "$output_path/z_z"
mkdir -p "$output_path/z_v"
mkdir -p "$output_path/z_g"
mkdir -p "$output_path/z_r"
mkdir -p "$output_path/z_delta"
mkdir -p "$output_path/h_hk"
mkdir -p "$output_path/h_runs"
mkdir -p "$output_path/h_z"
mkdir -p "$output_path/h_v"
mkdir -p "$output_path/h_g"
mkdir -p "$output_path/h_r"
mkdir -p "$output_path/h_delta"

i=0
while IFS= read -r line
do
	set -- $line
		file_inputs=$1
		file_path=$2
		output_file=$3
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup ./compute_hk "$origin_path/n_rm_$output_file.bin" > "$output_path/rm_hk/$output_result" &
		nohup ./compute_runs "$origin_path/n_rm_$output_file.bin" > "$output_path/rm_runs/$output_result" &
		nohup ./compute_z -f "$origin_path/n_rm_$output_file.bin" > "$output_path/rm_z/$output_result" &
		nohup ./compute_v "$origin_path/n_rm_$output_file.bin" > "$output_path/rm_v/$output_result" &
		nohup ./bal/irepair "$origin_path/n_rm_$output_file.bin" > "$output_path/rm_g/$output_result" &
		nohup ./compute_r "$origin_path/n_rm_$output_file.bin" > "$output_path/rm_r/$output_result" &
		nohup ./compute_delta "$origin_path/n_rm_$output_file.bin" > "$output_path/rm_delta/$output_result" &
		
		nohup ./compute_hk "$origin_path/n_z_$output_file.bin" > "$output_path/z_hk/$output_result" &
		nohup ./compute_runs "$origin_path/n_z_$output_file.bin" > "$output_path/z_runs/$output_result" &
		nohup ./compute_z -f "$origin_path/n_z_$output_file.bin" > "$output_path/z_z/$output_result" &
		nohup ./compute_v "$origin_path/n_z_$output_file.bin" > "$output_path/z_v/$output_result" &
		nohup ./bal/irepair "$origin_path/n_z_$output_file.bin" > "$output_path/z_g/$output_result" &
		nohup ./compute_r "$origin_path/n_z_$output_file.bin" > "$output_path/z_r/$output_result" &
		nohup ./compute_delta "$origin_path/n_z_$output_file.bin" > "$output_path/z_delta/$output_result" &
		
		nohup ./compute_hk "$origin_path/n_h_$output_file.bin" > "$output_path/h_hk/$output_result" &
		nohup ./compute_runs "$origin_path/n_h_$output_file.bin" > "$output_path/h_runs/$output_result" &
		nohup ./compute_z -f "$origin_path/n_h_$output_file.bin" > "$output_path/h_z/$output_result" &
		nohup ./compute_v "$origin_path/n_h_$output_file.bin" > "$output_path/h_v/$output_result" &
		nohup ./bal/irepair "$origin_path/n_h_$output_file.bin" > "$output_path/h_g/$output_result" &
		nohup ./compute_r "$origin_path/n_h_$output_file.bin" > "$output_path/h_r/$output_result" &
		nohup ./compute_delta "$origin_path/n_h_$output_file.bin" > "$output_path/h_delta/$output_result" &
		
		i=$(($i + 1))
done < $list_files