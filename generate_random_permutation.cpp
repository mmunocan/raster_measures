/*
 * generate the random permutation file
 */

#include <iostream>
#include <algorithm>
#include <fstream>
#include <cassert>

using namespace std;

void generate_random_permutation(size_t n_rows, size_t n_cols, const string &output_filename){
	size_t n = n_rows * n_cols;
	
	unsigned int *data = new unsigned int[n];
	for(size_t i = 0; i < n; i++){
		data[i] = i;
	}
	
	// shuffle cells
	random_shuffle(data, data+n);
	
	// Write the output
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)data, n*sizeof(unsigned int));
	output_file.close();
	
}

int main(int argc, char ** argv){
	
	if(argc != 4){
		cerr << "ERROR! USE " << argv[0] << " <n_rows> <n_cols> <output_filename> " << endl;
		return -1;
	}
	
	unsigned int n_rows = atoi(argv[1]);
	unsigned int n_cols = atoi(argv[2]);
	string output_filename = argv[3];
	
	generate_random_permutation(n_rows, n_cols, output_filename);
	
	cout << "File " << output_filename << " generated." << endl;
	
	return 0;
}