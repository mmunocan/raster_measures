'''
 Given a int32 raster, compute Geary' C
'''
import sys
import numpy as np
from esda.geary import Geary
from libpysal.weights import W, lat2W

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <filename> <n_rows> <n_cols>")
	sys.exit()

filename = sys.argv[1]
n_rows = int(sys.argv[2])
n_cols = int(sys.argv[3])


file_raster = open(filename, "rb")
raster_type = np.dtype((np.int32, (n_rows, n_cols)))
raster = np.fromfile(file_raster, dtype=raster_type)[0]
file_raster.close()

w = lat2W(n_rows, n_cols, rook=False)
lm = Geary(raster, w)
print("{};{:.10f}".format(filename, lm.C))
