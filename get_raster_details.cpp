/* 
 * Given a raster, print the basicals details of the rasters:
 * [min value, max value, uniques values and % alphabet used]
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <map>
#include <climits>
#include <iomanip>

using namespace std;

void get_details(const string & input_filename, unsigned int n_rows, unsigned int n_cols){
	
	// Reading the input
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(unsigned int);
	input_file.seekg(0, ios::beg);
	
	unsigned int * data = new unsigned int[n];
	
	input_file.read((char*)data, n * sizeof(unsigned int));
	input_file.close();
	
	map<unsigned int, size_t> frequencies;
	
	unsigned int minimum = INT_MAX;
	unsigned int maximum = 0;
	unsigned int value = 0;
	for(size_t i = 0; i < n; i++){
		value = data[i];
		if(frequencies.count(value)) frequencies[value]++;
		else frequencies[value] = 1;
		
		if(value > maximum) maximum = value;
		if(value < minimum) minimum = value;
		
	}
	
	size_t uniques = frequencies.size();
	double percentaje = (double)uniques/(double)(maximum-minimum+1)*100;
	
	cout << fixed << setprecision(3);
	/*
	cout << "Filename: " << input_filename << endl;
	cout << "Min value: " << minimum << endl;
	cout << "Max value: " << maximum << endl;
	cout << "Unique values: " << uniques << endl;
	cout << "% Uniques / total values: " << percentaje << endl;
	*/
	
	cout << input_filename << ";" <<  n_rows << ";" << n_cols << ";" << minimum << ";" << maximum << ";" << uniques << ";" << percentaje << endl;
	
}

int main(int argc, char ** argv){
	if(argc != 4){
		cerr << "ERROR! USE " << argv[0] << " <input_filename>  <n_rows> <n_cols>" << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	unsigned int n_rows = atoi(argv[2]);
	unsigned int n_cols = atoi(argv[3]);
	
	get_details(input_filename, n_rows, n_cols);
	
	return 0;
}