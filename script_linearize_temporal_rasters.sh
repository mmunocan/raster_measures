#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <output_path> <list_files>"
	exit
fi

output_path=$1
list_files=$2

mkdir -p $output_path

i=0
while IFS= read -r line
do
	set -- $line
		file_inputs=$1
		file_path=$2
		output_file=$3
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		./temporal_raster_linearize_row_major $file_inputs $file_path "$output_path/rm_$output_file.bin" 
		./temporal_raster_linearize_zorder $file_inputs $file_path "$output_path/z_$output_file.bin" 
		./temporal_raster_linearize_hilbert $file_inputs $file_path "$output_path/h_$output_file.bin" 
		
		./continuous_alphabet "$output_path/rm_$output_file.bin" "$output_path/n_rm_$output_file.bin" 
		./continuous_alphabet "$output_path/z_$output_file.bin" "$output_path/n_z_$output_file.bin" 
		./continuous_alphabet "$output_path/h_$output_file.bin" "$output_path/n_h_$output_file.bin" 
		
		i=$(($i + 1))
done < $list_files