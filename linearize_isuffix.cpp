#include <iostream>
#include <map>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cassert>

using namespace std;

void linearize_isuffix(const string & input_filename, size_t n_rows, size_t n_cols, const string & output_filename){
	// Read the input
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	size_t total_cells = n_rows * n_cols;
	unsigned int * data = new unsigned int[total_cells];
	
	input_file.read((char*)data, total_cells * sizeof(unsigned int));
	input_file.close();
	
	// Linearization
	size_t n = n_rows < n_cols ? n_rows : n_cols;
	size_t pos = 0;
	unsigned int * isuffix = new unsigned int[total_cells];
	
	for(size_t i = 0; i < n; i++){
		// Horizontal icharacter
		for(size_t j = i; j < n_cols; j++){
			isuffix[pos] = data[(n_cols*i)+j];
			pos++;
		}
		// Vertical icharacter
		for(size_t j = i+1; j < n_rows; j++){
			isuffix[pos] = data[(n_cols*j)+i];
			pos++;
		}
	}
	
	// Excedent
	if(n_rows < n_cols){
		for(size_t i = n_rows; i < n_cols; i++){
			for(size_t j = 0; j < n_rows; j++){
				isuffix[pos] = data[(n_cols*j)+i];
				pos++;
			}
		}
	}else if(n_cols < n_rows){
		for(size_t i = n_cols; i < n_rows; i++){
			for(size_t j = 0; j < n_cols; j++){
				isuffix[pos] = data[(n_cols*i)+j];
				pos++;
			}
		}
	}
	
	// Write the output
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)isuffix, total_cells * sizeof(unsigned int));
	output_file.close();
	
}

int main(int argc, char **argv){
	
	if(argc != 5){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <n_rows> <n_cols> <output_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	unsigned int n_rows = atoi(argv[2]);
	unsigned int n_cols = atoi(argv[3]);
	string output_filename = argv[4];
	
	linearize_isuffix(input_filename, n_rows, n_cols, output_filename);
	
	cout << "File " << output_filename << " from " << input_filename << " ready! " << endl;
	
	return 0;
}