/*
 * Given a raster, transform it from the original alphabet to continuous alphabet,
 * to a range [1..\sigma]
 */

#include <iostream>
#include <vector>
#include <fstream>
#include <cassert>
#include <string>
#include <map>
#include <algorithm>

using namespace std;

void preprocess_text(const string & input_filename, const string & output_filename){
	// Reading the input
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(unsigned int);
	input_file.seekg(0, ios::beg);
	
	unsigned int * data = new unsigned int[n];
	
	input_file.read((char*)data, n * sizeof(unsigned int));
	input_file.close();
	
	// Get all symbols
	map<unsigned int, unsigned int> symbols;
	for(size_t i = 0; i < n; i++){
		if(symbols.count(data[i]) == 0){
			symbols[data[i]] = 1;
		}
	}
	
	// sort symbols
	vector<unsigned int> sorted_symbols;
	for(auto it : symbols){
		sorted_symbols.push_back(it.first);
	}
	sort(sorted_symbols.begin(), sorted_symbols.end());
	
	// assign new symbols
	map<unsigned int, unsigned int> dictionary;
	unsigned int s = 1;
	for(unsigned int c : sorted_symbols){
		dictionary[c] = s;
		s++;
	}
	
	// generate new text
	unsigned int * output = new unsigned int[n];
	for(size_t i = 0; i < n; i++){
		output[i] = dictionary[data[i]];
	}
	
	// write new text
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)output, n * sizeof(unsigned int));
	output_file.close();
	
}

int main(int argc, char ** argv){
	if(argc != 3){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <output_filename>" << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	string output_filename = argv[2];
	
	preprocess_text(input_filename, output_filename);
	
	cout << "File " << output_filename << " from " << input_filename << " ready! " << endl;
	
	return 0;
}