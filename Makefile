CPP=g++

OBJECTS=
		
BINS=get_raster_details get_structures_sizes generate_random_permutation continuous_alphabet linearize_zorder linearize_hilbert linearize_random linearize_isuffix temporal_raster_linearize_row_major temporal_raster_linearize_zorder temporal_raster_linearize_hilbert generate_temporal_raster_repeated compute_hk compute_runs compute_z compute_v compute_r compute_delta compute_delta_delta compute_delta_2d compute_locality compute_netcdf compute_context compute_context_variance
		
CPPFLAGS=-fopenmp -O9 -g3 -std=c++17 -march=native -mtune=native -mavx2 -funroll-loops -finline-functions -fomit-frame-pointer -flto -Wall -lm -DNDEBUG -I ~/include -L ~/lib -lnetcdf
CPPFLAGSB=-lsdsl -ldivsufsort -ldivsufsort64 
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)

get_raster_details:
	g++ $(CPPFLAGS) -o $(DEST)/get_raster_details get_raster_details.cpp $(OBJECTS) $(CPPFLAGSB) 
	
get_structures_sizes:
	g++ $(CPPFLAGS) -o $(DEST)/get_structures_sizes get_structures_sizes.cpp $(OBJECTS) $(CPPFLAGSB) 
	
generate_random_permutation:
	g++ $(CPPFLAGS) -o $(DEST)/generate_random_permutation generate_random_permutation.cpp $(OBJECTS) $(CPPFLAGSB) 
	
continuous_alphabet:
	g++ $(CPPFLAGS) -o $(DEST)/continuous_alphabet continuous_alphabet.cpp $(OBJECTS) $(CPPFLAGSB) 
	
linearize_zorder:
	g++ $(CPPFLAGS) -o $(DEST)/linearize_zorder linearize_zorder.cpp $(OBJECTS) $(CPPFLAGSB) 
	
linearize_hilbert:
	g++ $(CPPFLAGS) -o $(DEST)/linearize_hilbert linearize_hilbert.cpp $(OBJECTS) $(CPPFLAGSB) 
	
linearize_random:
	g++ $(CPPFLAGS) -o $(DEST)/linearize_random linearize_random.cpp $(OBJECTS) $(CPPFLAGSB) 

linearize_isuffix:
	g++ $(CPPFLAGS) -o $(DEST)/linearize_isuffix linearize_isuffix.cpp $(OBJECTS) $(CPPFLAGSB) 

temporal_raster_linearize_row_major:
	g++ $(CPPFLAGS) -o $(DEST)/temporal_raster_linearize_row_major temporal_raster_linearize_row_major.cpp $(OBJECTS) $(CPPFLAGSB) 
	
temporal_raster_linearize_zorder:
	g++ $(CPPFLAGS) -o $(DEST)/temporal_raster_linearize_zorder temporal_raster_linearize_zorder.cpp $(OBJECTS) $(CPPFLAGSB) 
	
temporal_raster_linearize_hilbert:
	g++ $(CPPFLAGS) -o $(DEST)/temporal_raster_linearize_hilbert temporal_raster_linearize_hilbert.cpp $(OBJECTS) $(CPPFLAGSB) 

generate_temporal_raster_repeated:
	g++ $(CPPFLAGS) -o $(DEST)/generate_temporal_raster_repeated generate_temporal_raster_repeated.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_hk:
	g++ $(CPPFLAGS) -o $(DEST)/compute_hk compute_hk.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_runs:
	g++ $(CPPFLAGS) -o $(DEST)/compute_runs compute_runs.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_z:
	g++ $(CPPFLAGS) -o $(DEST)/compute_z compute_z.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_v:
	g++ $(CPPFLAGS) -o $(DEST)/compute_v compute_v.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_r:
	g++ $(CPPFLAGS) -o $(DEST)/compute_r compute_r.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_delta:
	g++ $(CPPFLAGS) -o $(DEST)/compute_delta compute_delta.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_delta_2d:
	g++ $(CPPFLAGS) -o $(DEST)/compute_delta_2d compute_delta_2d.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_delta_delta:
	g++ $(CPPFLAGS) -o $(DEST)/compute_delta_delta compute_delta_delta.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_locality:
	g++ $(CPPFLAGS) -o $(DEST)/compute_locality compute_locality.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_netcdf:
	g++ $(CPPFLAGS) -o $(DEST)/compute_netcdf compute_netcdf.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_context:
	g++ $(CPPFLAGS) -o $(DEST)/compute_context compute_context.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_context_variance:
	g++ $(CPPFLAGS) -o $(DEST)/compute_context_variance compute_context_variance.cpp $(OBJECTS) $(CPPFLAGSB) 

clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f *.a $(BINS)
