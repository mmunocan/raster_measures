/*
 * Given a raster, linearize it using Morton Curve Curve
 */

#include <iostream>
#include <map>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cassert>

using namespace std;


unsigned int get_side(unsigned int rows, unsigned int cols){
	if(rows >= cols){
		return  (1 << (32-__builtin_clz(rows-1)));
	}else{
		return  (1 << (32-__builtin_clz(cols-1)));
	}
}

unsigned long interleave_uint32_with_zeros(unsigned int input)  {
	unsigned long word = input;
	word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
	word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
	word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
	word = (word ^ (word << 2 )) & 0x3333333333333333;
	word = (word ^ (word << 1 )) & 0x5555555555555555;
	return word;
}

unsigned long get_zorder(unsigned int p, unsigned int q){
	return interleave_uint32_with_zeros(q) | (interleave_uint32_with_zeros(p) << 1);
}


void linearize_zorder(const string & input_filename, size_t n_rows, size_t n_cols, const string & output_filename){
	// Read the input
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	size_t total_cells = n_rows * n_cols;
	unsigned int * data = new unsigned int[total_cells];
	
	input_file.read((char*)data, total_cells * sizeof(unsigned int));
	input_file.close();
	
	// Linearizate in zorder
	unsigned int side = get_side(n_rows, n_cols);
	unsigned int double_side = side * side;
	vector<unsigned int> zorder(double_side, 0);
	vector<bool> is_ready(double_side, false);
	
	size_t index = 0;
	size_t current_cell = 0;
	for(size_t r = 0; r < n_rows; r++){
		for(size_t c = 0; c < n_cols; c++){
			index = get_zorder(r, c);
			zorder[index] = data[current_cell];
			is_ready[index] = true;
			current_cell++;
		}
	}
	
	unsigned int * new_zorder = new unsigned int[total_cells];
	index = 0;
	for(size_t i = 0; i < double_side; i++){
		if(is_ready[i]){
			new_zorder[index] = zorder[i];
			index++;
		}
	}
	
	// Write the output
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)new_zorder, total_cells * sizeof(unsigned int));
	output_file.close();
	
}

int main(int argc, char ** argv){
	
	if(argc != 5){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <n_rows> <n_cols> <output_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	unsigned int n_rows = atoi(argv[2]);
	unsigned int n_cols = atoi(argv[3]);
	string output_filename = argv[4];
	
	linearize_zorder(input_filename, n_rows, n_cols, output_filename);
	
	cout << "File " << output_filename << " from " << input_filename << " ready! " << endl;
	
	return 0;
}