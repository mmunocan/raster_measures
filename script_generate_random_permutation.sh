#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3


i=0
while IFS= read -r line
do
	set -- $line
		n_rows=$1
		n_cols=$2
		filename=$3
		
		nohup ./generate_random_permutation $n_rows $n_cols "$output_path/$filename" &
		
		i=$(($i + 1))
done < $list_files