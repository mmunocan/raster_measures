#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3

mkdir -p $output_path
mkdir -p "$output_path/rm_hk"
mkdir -p "$output_path/rm_runs"
mkdir -p "$output_path/rm_z"
mkdir -p "$output_path/rm_v"
mkdir -p "$output_path/rm_g"
mkdir -p "$output_path/rm_r"
mkdir -p "$output_path/rm_delta"
mkdir -p "$output_path/z_hk"
mkdir -p "$output_path/z_runs"
mkdir -p "$output_path/z_z"
mkdir -p "$output_path/z_v"
mkdir -p "$output_path/z_g"
mkdir -p "$output_path/z_r"
mkdir -p "$output_path/z_delta"
mkdir -p "$output_path/h_hk"
mkdir -p "$output_path/h_runs"
mkdir -p "$output_path/h_z"
mkdir -p "$output_path/h_v"
mkdir -p "$output_path/h_g"
mkdir -p "$output_path/h_r"
mkdir -p "$output_path/h_delta"
mkdir -p "$output_path/rd_hk"
mkdir -p "$output_path/rd_runs"
mkdir -p "$output_path/rd_z"
mkdir -p "$output_path/rd_v"
mkdir -p "$output_path/rd_g"
mkdir -p "$output_path/rd_r"
mkdir -p "$output_path/rd_delta"
mkdir -p "$output_path/delta2d"
mkdir -p "$output_path/deltadelta"
mkdir -p "$output_path/moran"
mkdir -p "$output_path/geary"
mkdir -p "$output_path/k2raster"
mkdir -p "$output_path/locality"
mkdir -p "$output_path/compressors"

i=0
while IFS= read -r line
do
	set -- $line
		filename=$1
		n_rows=$2
		n_cols=$3
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		
		# measures 1d
		./compute_hk "$origin_path/rm_$filename" > "$output_path/rm_hk/$output_result"
		./compute_runs "$origin_path/rm_$filename" > "$output_path/rm_runs/$output_result"
		./compute_z -f "$origin_path/rm_$filename" > "$output_path/rm_z/$output_result"
		./compute_v "$origin_path/rm_$filename" > "$output_path/rm_v/$output_result"
		./bal/irepair "$origin_path/rm_$filename" > "$output_path/rm_g/$output_result"
		./compute_r "$origin_path/rm_$filename" > "$output_path/rm_r/$output_result"
		./compute_delta "$origin_path/rm_$filename" > "$output_path/rm_delta/$output_result"
		
		./compute_hk "$origin_path/z_$filename" > "$output_path/z_hk/$output_result"
		./compute_runs "$origin_path/z_$filename" > "$output_path/z_runs/$output_result"
		./compute_z -f "$origin_path/z_$filename" > "$output_path/z_z/$output_result"
		./compute_v "$origin_path/z_$filename" > "$output_path/z_v/$output_result"
		./bal/irepair "$origin_path/z_$filename" > "$output_path/z_g/$output_result"
		./compute_r "$origin_path/z_$filename" > "$output_path/z_r/$output_result"
		./compute_delta "$origin_path/z_$filename" > "$output_path/z_delta/$output_result"
		
		./compute_hk "$origin_path/h_$filename" > "$output_path/h_hk/$output_result"
		./compute_runs "$origin_path/h_$filename" > "$output_path/h_runs/$output_result"
		./compute_z -f "$origin_path/h_$filename" > "$output_path/h_z/$output_result"
		./compute_v "$origin_path/h_$filename" > "$output_path/h_v/$output_result"
		./bal/irepair "$origin_path/h_$filename" > "$output_path/h_g/$output_result"
		./compute_r "$origin_path/h_$filename" > "$output_path/h_r/$output_result"
		./compute_delta "$origin_path/h_$filename" > "$output_path/h_delta/$output_result"
		
		./compute_hk "$origin_path/rd_$filename" > "$output_path/rd_hk/$output_result"
		./compute_runs "$origin_path/rd_$filename" > "$output_path/rd_runs/$output_result"
		./compute_z -f "$origin_path/rd_$filename" > "$output_path/rd_z/$output_result"
		./compute_v "$origin_path/rd_$filename" > "$output_path/rd_v/$output_result"
		./bal/irepair "$origin_path/rd_$filename" > "$output_path/rd_g/$output_result"
		./compute_r "$origin_path/rd_$filename" > "$output_path/rd_r/$output_result"
		./compute_delta "$origin_path/rd_$filename" > "$output_path/rd_delta/$output_result"
		
		# measures 2d
		./compute_delta_2d "$origin_path/$filename" $n_rows $n_cols > "$output_path/delta2d/$output_result"
		./compute_delta_delta "$origin_path/$filename" $n_rows $n_cols > "$output_path/deltadelta/$output_result"
		
		# compressors
		bzip2 -k "$origin_path/$filename"
		gzip -k "$origin_path/$filename"
		./compute_netcdf "$origin_path/$filename" $n_rows $n_cols
		./k2raster/build/bin/encode_k2r "$origin_path/$filename" $n_rows $n_cols "$origin_path/$filename.k2r" > "$output_path/k2raster/$output_result"
		
		./get_structures_sizes "$origin_path/$filename" > "$output_path/compressors/$output_result"
		
		# spatial indexs
		python3 moran.py "$origin_path/$filename" $n_rows $n_cols > "$output_path/moran/$output_result"
		python3 geary.py "$origin_path/$filename" $n_rows $n_cols > "$output_path/geary/$output_result"
		
		./compute_locality "$origin_path/$filename" $n_rows $n_cols > "$output_path/locality/$output_result"
		
		i=$(($i + 1))
done < $list_files