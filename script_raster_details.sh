#!/usr/bin/bash

if (($# != 3));
then
	echo "Error! include <origin_path> <output_path> <list_files>"
	exit
fi

origin_path=$1
output_path=$2
list_files=$3

mkdir -p $output_path
mkdir -p "$output_path/information"

i=0
while IFS= read -r line
do
	set -- $line
		filename=$1
		n_rows=$2
		n_cols=$3
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup ./get_raster_details "$origin_path/$filename" $n_rows $n_cols > "$output_path/information/$output_result" &
		
		i=$(($i + 1))
done < $list_files