/* 
 * Given a int raster, compute \delta_{\Delta} measure
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <map>
#include <utility>
#include <climits>
#include <cmath>
#include <chrono>

using namespace std;

void read_dataset(const string & filename, vector<vector<int>> & raster){
	
	size_t n_rows = raster.size();
	size_t n_cols = raster[0].size();
	
	ifstream file;
	file.open(filename, ios::binary);
	assert(file.is_open() && file.good());
	
	int value;
	for(size_t i = 0; i < n_rows; i++){
		for(size_t j = 0; j < n_cols; j++){
			file.read((char *)(& value), sizeof(int));
			raster[i][j] = value;
		}
	}
	file.close();
	
}

/*
 * Given an squared submatrix [i..i+k-1]x[j..j+k-1], generates the string equivalent
 * that covers the submatrix per each byte in row-major
 */
string get_submatrix(const vector<vector<int>> & raster, size_t i, size_t j, size_t k){
	
	size_t n = k * k;
	int * submatrix = new int[n];
	
	for(size_t l = 0; l < k; l++){
		for(size_t m = 0; m < k; m++){
			submatrix[(l*k)+m] = raster[i+l][j+m];
		}
	}
	
	char * submatrix_chr = (char*) submatrix;
	string submatrix_str;
	submatrix_str.assign(submatrix_chr, submatrix_chr + n*sizeof(int));
	return submatrix_str;
	
}

unsigned long long compute_sum(const vector<vector<int>> & raster, size_t k){
	
	size_t n_rows = raster.size();
	size_t n_cols = raster[0].size();
	map<string, size_t> frequencies;
	unsigned long long sum = 0;
	int min_val, max_val;
	
	for(size_t i = 0; i < (n_rows - k + 1); i++){
		for(size_t j = 0; j < (n_cols - k + 1); j++){
			string submatrix = get_submatrix(raster, i, j, k);
			if(!frequencies.count(submatrix)){
				frequencies[submatrix] = 1;
				min_val = INT_MAX;
				max_val = INT_MIN;
				for(size_t l = 0; l < k; l++){
					for(size_t m = 0; m < k; m++){
						min_val = min(min_val, raster[i+l][j+m]);
						max_val = max(max_val, raster[i+l][j+m]);
					}
				}
				sum += (unsigned long long)(max_val-min_val);
			}
		}
	}
	return sum;
	
}

double computing_fraction(const vector<vector<int>> & raster, size_t k){
	unsigned long long numer = compute_sum(raster, k);
	unsigned long long denom = (unsigned long long)k*k;
	//cout << numer << "/" << denom << endl;
	return (double)((double)numer/denom);
}

pair<double, size_t> compute_delta_delta_measure(const string & filename, size_t n_rows, size_t n_cols){
	vector<vector<int>> raster(n_rows, vector<int>(n_cols));
	read_dataset(filename, raster);
	
	size_t min_size = min(n_rows,n_cols);
	vector<double> d_k(min_size+1);
	d_k[0] = 0;
	
	for(size_t k = 1; k <= min_size; k++){
		d_k[k] = computing_fraction(raster, k);
		if(k != 1 && d_k[k-1] > d_k[k]){
			return make_pair(d_k[k-1], k-1);
		}
	}
	return make_pair(d_k[1], 0);
}

int main(int argc, char ** argv){
	
	if(argc != 4){
		cout << "ERROR! USE " << argv[0] << " <filename> <n_rows> <n_cols> " << endl;
		return -1;
	}
	
	string filename = argv[1];
	size_t n_rows = atoi(argv[2]);
	size_t n_cols = atoi(argv[3]);
	
	auto start = chrono::high_resolution_clock::now();
	pair<double, size_t> delta = compute_delta_delta_measure(filename, n_rows, n_cols);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << fixed;
	cout << filename << ";" << delta.first << ";" << delta.second << ";" << time << endl;
	
	return 0;
}