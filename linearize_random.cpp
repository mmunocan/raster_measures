/*
 * Given a raster and a random permutation, shuffle the values generating an 1D sequence with
 * the random permutation given
 */

#include <iostream>
#include <fstream>
#include <cassert>

using namespace std;

void linearize_random(const string & input_filename, const string & permutation_filename, const string & output_filename){
	// Read the input
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	unsigned int *data = new unsigned int[n];
	input_file.read((char*)data, n*sizeof(unsigned int));
	input_file.close();
	
	// Read permutation file
	ifstream permutation_file;
	permutation_file.open(permutation_filename, ios::binary);
	assert(permutation_file.is_open() && permutation_file.good());
	
	unsigned int *permutation = new unsigned int[n];
	permutation_file.read((char*)permutation, n*sizeof(unsigned int));
	permutation_file.close();
	
	// Generate the output
	unsigned int *output = new unsigned int[n];
	for(size_t i = 0; i < n; i++){
		output[i] = data[permutation[i]];
	}
	
	// Write the output
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)output, n*sizeof(unsigned int));
	output_file.close();
	
}

int main(int argc, char ** argv){
	
	if(argc != 4){
		cerr << "ERROR! USE " << argv[0] << " <input_filename> <permutation_filename> <output_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	string permutation_filename = argv[2];
	string output_filename = argv[3];
	
	linearize_random(input_filename, permutation_filename, output_filename);
	
	return 0;
}