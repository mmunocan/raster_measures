/* 
 * Given a int raster, compute locality measure
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>

using namespace std;

void read_dataset(const string & filename, vector<vector<int>> & raster){
	
	size_t n_rows = raster.size();
	size_t n_cols = raster[0].size();
	
	ifstream file;
	file.open(filename, ios::binary);
	assert(file.is_open() && file.good());
	
	int value;
	for(size_t i = 0; i < n_rows; i++){
		for(size_t j = 0; j < n_cols; j++){
			file.read((char *)(& value), sizeof(int));
			raster[i][j] = value;
		}
	}
	file.close();
	
}

size_t differents_neighbors(const vector<vector<int>> & raster, int r, int c){
	int n_rows = raster.size();
	int n_cols = raster[0].size();
	
	int cell = raster[r][c];
	size_t total = 0;
	
	if(0 <= r-1 && 0 <= c-1)			total += raster[r-1][c-1] == cell;
	if(0 <= r-1)						total += raster[r-1][c] == cell;
	if(0 <= r-1 && c+1 < n_cols)		total += raster[r-1][c+1] == cell;
	if(c+1 < n_cols)					total += raster[r][c+1] == cell;
	if(r+1 < n_rows && c+1 < n_cols)	total += raster[r+1][c+1] == cell;
	if(r+1 < n_rows)					total += raster[r+1][c] == cell;
	if(r+1 < n_rows && 0 <= c-1)		total += raster[r+1][c-1] == cell;
	if(0 <= c-1)						total += raster[r][c-1] == cell;
	
	return total;
}

int main(int argc, char ** argv){
	
	if(argc != 4){
		cout << "ERROR! USE " << argv[0] << " <filename> <n_rows> <n_cols> " << endl;
		return -1;
	}
	
	string filename = argv[1];
	size_t n_rows = atoi(argv[2]);
	size_t n_cols = atoi(argv[3]);
	
	vector<vector<int>> raster(n_rows, vector<int>(n_cols));
	read_dataset(filename, raster);
	
	size_t total = 0;
	
	for(size_t r = 0; r < n_rows; r++){
		for(size_t c = 0; c < n_cols; c++){
			total += differents_neighbors(raster, r, c);
		}
	}
	
	double result = (double)total / (double)(n_rows*n_cols);
	
	cout << filename << ";" << result << endl;
	
	return 0;
}