/* 
 * Given a int raster, compute z measure
 */

#include <iostream>
#include <chrono>
#include <vector>
#include <fstream>
#include <cassert>
#include <algorithm>

#include <sys/stat.h>

#include <sdsl/lcp.hpp>
#include <sdsl/suffix_arrays.hpp>

#define PSV(i) pnsv[(i << 1)]
#define NSV(i) pnsv[(i << 1)+1]

using namespace std;
using namespace sdsl;

enum FLAGS {
	DOUBLE_SA = 1 // allocate double required memory for suffix array
};

bool checkResult = false;

inline int int_naiveLCP(const unsigned int * const x, int i, int j, int n){
	int l = 0;
	while(j < n && x[i++] == x[j++]){ l++; }
	return l;
}

void print_usage(int argc, char * argv []){
	cout << "Usage  : " << argv[0] << " [options]" << endl
		<< "Options: " << endl
		<< "  -f iFile : file to process" << endl
		<< "  -x       : use iFile + '.sa' for suffix array cache" << endl
		<< "  -g       : check if resulting factorization produces input string" << endl;
	return;
}

void int_lzFromLOPNSV(const vector<unsigned int> & s, 
						const int * sa,
						const int * rank,
						const int * pnsv,
						vector<pair<int,int> > & lz){

	////////////////////////////////////////////////////////////
	// main LZ factorization
	////////////////////////////////////////////////////////////
	size_t p = 1;
	lz.clear();
	lz.push_back(make_pair(0, s[0]));
	while(p < s.size()){
		int i = rank[p];
		int prevPos = sa[PSV(i)];
		int lpf = (PSV(i) < 0) ? 0 : int_naiveLCP(s.data(), sa[PSV(i)], p, s.size());
		int nlen = (NSV(i) < 0) ? 0 : int_naiveLCP(s.data(), sa[NSV(i)], p, s.size());
		if(nlen > lpf){ lpf = nlen; prevPos = sa[NSV(i)]; }
		if(lpf > 0){
			lz.push_back(make_pair(lpf,prevPos));
			p += lpf;
		} else {
			lz.push_back(make_pair(0, s[p]));
			p++;
		}
	}
	return;
	////////////////////////////////////////////////////////////
}

int * int_suffixArray(const vector<unsigned int> & s, int * sa, unsigned int f, string & filename_data){
	if(sa == 0){
		size_t sasize = (f & DOUBLE_SA) ? s.size() * 2 : s.size();
		sa = new int[sasize];
	}
	
	
	csa_wt<wt_int<>> SA_compress;
	construct(SA_compress, filename_data, sizeof(int)); 
	
	for(size_t i = 0; i < SA_compress.size()-1; i++){
		sa[i] = SA_compress[i+1];
	}
	
	return (sa);
}

int * int_saFromFile(const vector<unsigned int> & s, const string & fname, int * sa, unsigned int f, string & filename_data){
	////////////////////////////////////////////////////////////
	// check if suffix array file if it exists
	////////////////////////////////////////////////////////////
	bool remake = false;
	struct stat st1, st2;
	string safname = fname + ".sa";
	if(stat(fname.c_str(), &st1)) remake = true;
	if(stat(safname.c_str(), &st2)) remake = true;
	if(st1.st_mtime >= st2.st_mtime){
		remake = true;
	}
	ifstream sfs(safname.c_str(), ios::in | ios::binary);
	if(!sfs){
		remake = true;
	} else if(sfs){
		const size_t fileSize = st2.st_size;
			if (fileSize != sizeof(int) * s.size()){
		remake = true;
		} else {
			cerr << "reading suffix array from: " << safname << flush;
			if(sa == 0){
				size_t sasize = (f & DOUBLE_SA) ? (fileSize / sizeof(int)) * 2 : fileSize / sizeof(int);
				sa = new int[sasize];
			}
			sfs.read(reinterpret_cast<char*>(sa), fileSize);
			cerr << " ...done" << endl;
		}
	}
	////////////////////////////////////////////////////////////
	// construct suffix array and cache it to file
	////////////////////////////////////////////////////////////
	if(remake){
		cerr << "suffix array file: " << safname << " not found, invalid or out of date." << endl;
		sa = int_suffixArray(s, sa, f, filename_data);
		cerr << "Saving suffix array to file..." << flush;
		ofstream ofs(safname.c_str(), ios::out | ios::binary | ios::trunc);
		ofs.write(reinterpret_cast<const char*>(sa), sizeof(int) * s.size());
		cerr << "done" << endl;
	}
	return(sa);
}

void int_FromFile(const string & fileName, vector<unsigned int> & s){
	ifstream ifs(fileName.c_str(), ios::in | ios::binary);
	if(!ifs){
		cerr << "failed to read file: " << fileName << endl;
		return;
	}

	ifs.seekg(0, ios::end);
	size_t n = ifs.tellg() / sizeof(int);
	ifs.seekg(0, ios::beg);

	int * data = new int[n];

	ifs.read((char*)data, n * sizeof(int));
	ifs.close();

	s.insert(s.begin(), data, data + n); 
	
}

int * int_Init(int argc, char * argv[], vector<unsigned int> & s, 
	     unsigned int f = 0, int *sa = 0){
	
	string filename(argv[2]);
	
	int ch;
	string inFile, saFile;
	bool useSAcache = false;
	while ((ch = getopt(argc, argv, "f:xgh")) != -1) {
		switch (ch) {
			case 'f':
				inFile = optarg;
				break;
			case 'x':
				useSAcache = true;
				break;
			case 'g':
				checkResult = true;
				break;
			default:
			print_usage(argc, argv);
			exit(0);
		}
	}
	if(inFile.empty()){ print_usage(argc, argv); exit(0); }

	argc -= optind;
	argv += optind;
	////////////////////////////////////////////////////////////
	
	
	
	int_FromFile(inFile, s);
	if(useSAcache){
		sa = int_saFromFile(s, inFile, sa, f, filename);
	} else {
		sa = int_suffixArray(s, sa, f, filename);
		
	}
	return(sa);
}

// this is a tail recursive call
void peakElim(int p, int c, int *pnsv, int *sa){  
	if(p < 0 || sa[p] < sa[c]){ 
		PSV(c) = p;
	} else { // sa[p] > sa[c] // p is peak
		NSV(p) = c;
		peakElim(PSV(p), c, pnsv, sa);
	}
}

int main(int argc, char ** argv){
	
	auto start = chrono::high_resolution_clock::now();
	vector<unsigned int> s;

	// parse options and read/construct string & suffix array
	int * sa = int_Init(argc, argv, s);
	////////////////////////////////////////////////////////////
	// calculate text order PSV, NSV using phi
	////////////////////////////////////////////////////////////
	int n = s.size(), i;
	int * rank = new int[n], * pnsv = new int[2*n];
	
	for(i = 0; i <= n; i++) rank[sa[i]] = i;
	
	for(int i = 0; i < n; i++) NSV(i) = -1;
	PSV(0) = -1;
	for(int i = 1; i < n; i++) peakElim(i-1, i, pnsv, sa);
	
	////////////////////////////////////////////////////////////
	// calculate LZ factorization from text order PSV, NSV
	////////////////////////////////////////////////////////////
	vector<pair<int,int> > lz;
	int_lzFromLOPNSV(s, sa, rank, pnsv, lz);
	
	
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();

	cout << argv[2] << ";" << lz.size() << ";" << time << endl;

	return 0;
}