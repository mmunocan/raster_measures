/*
 * Given a temporal raster, linearize it using Morton Curve
 */

#include <iostream>
#include <chrono>
#include <fstream>
#include <cassert>
#include <vector>

using namespace std;

unsigned int get_side(unsigned int rows, unsigned int cols){
	if(rows >= cols){
		return  (1 << (32-__builtin_clz(rows-1)));
	}else{
		return  (1 << (32-__builtin_clz(cols-1)));
	}
}

unsigned long interleave_uint32_with_zeros(unsigned int input)  {
	unsigned long word = input;
	word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
	word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
	word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
	word = (word ^ (word << 2 )) & 0x3333333333333333;
	word = (word ^ (word << 1 )) & 0x5555555555555555;
	return word;
}

unsigned long get_zorder(unsigned int p, unsigned int q){
	return interleave_uint32_with_zeros(q) | (interleave_uint32_with_zeros(p) << 1);
}

void linearize_zorder(const int * data, size_t n_rows, size_t n_cols, int * raster_values){
	// Linearizate in zorder
	unsigned int side = get_side(n_rows, n_cols);
	unsigned int double_side = side * side;
	vector<int> zorder(double_side);
	vector<bool> is_ready(double_side, false);
	
	size_t index = 0;
	size_t current_cell = 0;
	for(size_t r = 0; r < n_rows; r++){
		for(size_t c = 0; c < n_cols; c++){
			index = get_zorder(r, c);
			zorder[index] = data[current_cell];
			is_ready[index] = true;
			current_cell++;
		}
	}
	
	index = 0;
	for(size_t i = 0; i < double_side; i++){
		if(is_ready[i]){
			raster_values[index] = zorder[i];
			index++;
		}
	}
	
}

int main(int argc, char ** argv){
	if(argc != 4){
		cerr << "ERROR! USE " << argv[0] << " <inputs_filename> <inputs_path_folder> <output_filename>" << endl;
		return -1;
	}
	
	string inputs_filename = argv[1];
	string inputs_path_folder = argv[2];
	string output_filename = argv[3];
	
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	size_t n_cells = n_rows*n_cols;
	
	ofstream output_file(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	
	string raster_input_filename;
	int * raster_values_input = new int[n_cells];
	int * raster_values_output = new int[n_cells];
	while(inputs_file >> raster_input_filename){
		raster_input_filename = inputs_path_folder + "/" + raster_input_filename;
		ifstream raster_input_file(raster_input_filename, ios::binary);
		assert(raster_input_file.is_open() && raster_input_file.good());
		
		raster_input_file.read((char*)raster_values_input, n_cells * sizeof(int));
		linearize_zorder(raster_values_input, n_rows, n_cols, raster_values_output);
		output_file.write((char*)raster_values_output, n_cells * sizeof(int));
		
		raster_input_file.close();
	}
	
	inputs_file.close();
	output_file.close();
	
	return 0;
}