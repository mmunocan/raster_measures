/* 
 * Given a 1D integer sequence, compute the number of size kś context with k \in [0,6]
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <iomanip>

#include <sdsl/suffix_trees.hpp>
#include <sdsl/wt_int.hpp>

using namespace std;
using namespace sdsl;

size_t contains_zeros(const string & filename){
	// read the input
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * data = new int[n];
	
	input_file.read((char*)data, n * sizeof(int));
	input_file.close();
	
	size_t count_zeros = 0;
	for(size_t i = 0; i < n; i++){
		count_zeros += data[i] == 0;
		if(data[i] == 0){
			cout << "data["<<i<<"]= " << data[i] << endl;
		}
	}
	
	return count_zeros;
}

void compute_context(const string & filename){
	cst_sct3<csa_wt<wt_int<>>> cst;
	construct(cst, filename, sizeof(int));
	
	cout << filename << ";";
	for(int k = 0; k < 7; k++){
		cout << get<1>(Hk(cst, k)) << ";";
	}
	cout << endl;
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	// Revising datasets looking for zeros
	size_t count_zeros = contains_zeros(filename);
	if(count_zeros > 0){
		cerr << filename << " contains " << count_zeros << " zeros" << endl;
		return -1;
	}
	
	compute_context(filename);
	
	return 0;
}