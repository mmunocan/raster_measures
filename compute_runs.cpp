/* 
 * Given a int raster, compute the amount of runs
 */

#include <iostream>
#include <chrono>
#include <fstream>
#include <cassert>

using namespace std;

size_t compute_runs(const string & filename){
	// Reading the input
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * data = new int[n];
	
	input_file.read((char*)data, n * sizeof(int));
	input_file.close();
	
	size_t runs = 1;
	for(size_t i = 1; i < n; i++){
		if(data[i-1] != data[i]){
			runs++;
		}
	}
	runs++;
	
	return runs;
}

int main(int argc, char ** argv){
	
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	
	auto start = chrono::high_resolution_clock::now();
	size_t runs = compute_runs(filename);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << runs << ";" << time << endl;
	
	return 0;
}