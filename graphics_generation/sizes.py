import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 4:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> <output_filename_empirical> <output_filename_theoretical>")
	sys.exit()
	
input_filename = sys.argv[1]
output_filename_empirical = sys.argv[2]
output_filename_theoretical = sys.argv[3]

df = pd.read_csv(input_filename)

labels_empirical = ['original', '$H_0$', 'runs', '$r$', '$g$', '$v$', '$z$', '$\delta$', '$\delta_{2D}$']
x_empirical = np.arange(len(labels_empirical))
labels_theoretical = ['$r$', '$g$', '$z$', '$\delta$', '$\delta_{2D}$']
x_theoretical = np.arange(len(labels_theoretical))

width = 0.9

result_empirical = []
result_empirical.append(np.mean(np.array(df['Original_rep'])))
result_empirical.append(np.mean(np.array(df['H0_rep'])))
result_empirical.append(np.mean(np.array(df['runs_rep'])))
result_empirical.append(np.mean(np.array(df['r_rep'])))
result_empirical.append(np.mean(np.array(df['g_rep'])))
result_empirical.append(np.mean(np.array(df['v_rep'])))
result_empirical.append(np.mean(np.array(df['z_rep'])))
result_empirical.append(np.mean(np.array(df['delta_rep'])))
result_empirical.append(np.mean(np.array(df['delta_2d_rep'])))
result_empirical = np.array(result_empirical) / (8*1024*1024)

result_theoretical = []
result_theoretical.append(np.mean(np.array(df['r_estimacion'])))
result_theoretical.append(np.mean(np.array(df['g_estimacion'])))
result_theoretical.append(np.mean(np.array(df['z_estimacion'])))
result_theoretical.append(np.mean(np.array(df['delta_estimacion'])))
result_theoretical.append(np.mean(np.array(df['delta_2d_estimacion'])))
result_theoretical = np.array(result_theoretical)

plt.style.use('seaborn-poster') 

fig, ax = plt.subplots(figsize=(7,5))
ax.grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
ax.bar(x_empirical, result_empirical, width)
ax.set_ylabel('Mean Size [MB]')
ax.set_xlabel('Measure')
ax.set_xticks(x_empirical)
ax.set_xticklabels(labels_empirical)
fig.tight_layout()
plt.savefig(output_filename_empirical)

fig, ax = plt.subplots(figsize=(6,5))
ax.grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
ax.bar(x_theoretical, result_theoretical, width)
ax.set_ylabel('Size (log scale)')
ax.set_xlabel('Measure')
ax.set_xticks(x_theoretical)
ax.set_xticklabels(labels_theoretical)
plt.yscale("log") 
fig.tight_layout()
plt.savefig(output_filename_theoretical)
