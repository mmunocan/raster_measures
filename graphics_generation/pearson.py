import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import pearsonr
import seaborn as sns
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <input_filename> <output_path> <output_filename>")
	sys.exit()

input_filename = sys.argv[1]
output_path = sys.argv[2]
output_filename = sys.argv[3]

df = pd.read_csv(input_filename)

datas1 = {'$H_0$': df['Row_Major+H0'],
		 '$H_1$': df['Row_Major+H1'],
		 '$H_2$': df['Row_Major+H2'],
		 '$H_3$': df['Row_Major+H3'],
		 '$H_4$': df['Row_Major+H4'],
		 '$H_5$': df['Row_Major+H5'],
		 '$H_6$': df['Row_Major+H6'],
		 'runs': df['Row_Major+runs'],
		 '$r$': df['Row_Major+r'],
		 '$g$': df['Row_Major+g'],
		 '$v$': df['Row_Major+v'],
		 '$z$': df['Row_Major+z'],
		 '$\delta$': df['Row_Major+delta'],
		 '$\delta_{2D}$': df['delta2d']}

datas1 = pd.DataFrame(datas1)
pearson1 = datas1.corr()
pvalues_pearson1 = datas1.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(pearson1.shape[0])
pearson1 = pearson1.iloc[-7:]
pvalues_pearson1 = pvalues_pearson1.iloc[-7:]

datas2 = {'$H_0$': df['Row_Major+H0'],
		 'runs': df['Row_Major+runs'],
		 '$r$': df['Row_Major+r'],
		 '$g$': df['Row_Major+g'],
		 '$v$': df['Row_Major+v'],
		 '$z$': df['Row_Major+z'],
		 '$\delta$': df['Row_Major+delta'],
		 '$\delta_{2D}$': df['delta2d'],
		 'bzip': df['bzip'],
		 'gzip': df['gzip'],
		 'NetCDF': df['netcdf'],
		 '$k^2$-raster': df['k2raster'],
		 'locality': df['locality'],
		 "Moran's I": df['Moran'],
		 "Geary's C": df['Geary']}

datas2 = pd.DataFrame(datas2)
pearson2 = datas2.corr()
pvalues_pearson2 = datas2.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(pearson2.shape[0])
pearson2 = pearson2.iloc[-7:]
pvalues_pearson2 = pvalues_pearson2.iloc[-7:]

plt.style.use('seaborn-poster') 

fig, ax = plt.subplots(1,2, figsize=(12, 4))
dataplot = sns.heatmap(pearson1, cmap="gist_gray", vmin=-1.0, vmax=1.0, annot=False, cbar=True, xticklabels=True, yticklabels=True, ax=ax[0], linewidths=0.5, linecolor='black')

dataplot = sns.heatmap(pearson2, cmap="gist_gray", vmin=-1.0, vmax=1.0, annot=False, cbar=False, xticklabels=True, yticklabels=True, ax=ax[1], linewidths=0.5, linecolor='black')
plt.tight_layout()
plt.savefig('{}/{}_pearson.png'.format(output_path,output_filename))

print('\n'+output_filename)
pd.options.display.float_format = '{:.5f}'.format
with pd.option_context('display.max_rows', None, 'display.max_columns', None,): print(pvalues_pearson1)
with pd.option_context('display.max_rows', None, 'display.max_columns', None,): print(pvalues_pearson2)

