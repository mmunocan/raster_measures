import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import style
import matplotlib.ticker as mtick

def graphic_matrix(df, column_name, percentajes_change, percentajes_range, x_change, x_range, path, type_):
	datas_test = pd.concat([df['% changes'], df['% range variaton'], df[column_name]], axis = 1)
	data_matrix = datas_test.pivot(index='% changes', columns='% range variaton', values=column_name)
	data_matrix = np.array(data_matrix.values.tolist())
	value_original = original.iloc[0][column_name]
	data_matrix = (data_matrix / value_original)

	fig, ax = plt.subplots(figsize=(8,5))

	sns.heatmap(data_matrix, cmap='gist_gray', annot=True, fmt='.0%', cbar=False, xticklabels=True, yticklabels=False, linewidths=0.5, linecolor='black') 
	ax.set_yticks(x_change)
	ax.set_yticklabels(['{:.2%}'.format(x) for x in percentajes_change])
	ax.set_xticks(x_range)
	ax.set_xticklabels(['{:.0%}'.format(x) for x in percentajes_range])
	ax.set_ylabel('% PCC')
	ax.set_xlabel('% PV')
	plt.yticks(rotation=0)
	fig.tight_layout()
	plt.savefig('{}/{}_{}.png'.format(path,column_name,type_))
	
def graphic_matrix_2(df, column_name, percentajes_change, percentajes_range, x_change, x_range, path, type_):
	datas_test = pd.concat([df['% changes'], df['% range variaton'], df[column_name]], axis = 1)
	data_matrix = datas_test.pivot(index='% changes', columns='% range variaton', values=column_name)
	data_matrix = np.array(data_matrix.values.tolist())
	value_original = original.iloc[0][column_name]
	
	fig, ax = plt.subplots(figsize=(6,5))

	sns.heatmap(data_matrix, cmap='gist_gray', annot=True, fmt='.2f', cbar=False, xticklabels=True, yticklabels=True, linewidths=0.5, linecolor='black') 
	ax.set_yticks(x_change)
	ax.set_yticklabels(['{:.2%}'.format(x) for x in percentajes_change])
	ax.set_xticks(x_range)
	ax.set_xticklabels(['{:.0%}'.format(x) for x in percentajes_range])
	ax.set_ylabel('% PCC')
	ax.set_xlabel('% PV')
	plt.yticks(rotation=0)
	fig.tight_layout()
	plt.savefig('{}/{}_{}.png'.format(path,column_name,type_))

if len(sys.argv) != 3:
	print("ERROR! USE " + sys.argv[0] + " <input_filename> <output_path>")
	sys.exit()

input_filename = sys.argv[1]
output_path = sys.argv[2]

sns.set(font_scale=1.5)

df = pd.read_csv(input_filename)

original = df[df['type'] == 'original']
complete = df[df['type'] == 'complete']
inner = df[df['type'] == 'inner']

percentajes_change = np.sort(np.array(complete['% changes'].dropna().unique()))
x_change = np.arange(len(percentajes_change)) + 0.5
percentajes_range = np.sort(np.array(complete['% range variaton'].dropna().unique()))
x_range = np.arange(len(percentajes_range)) + 0.5

plt.style.use('seaborn-poster') 

graphic_matrix(complete, 'Row_Major+runs', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Row_Major+z', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Row_Major+v', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Row_Major+g', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Row_Major+r', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Row_Major+delta', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Zorder+runs', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Zorder+z', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Zorder+v', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Zorder+g', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Zorder+r', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Zorder+delta', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Hilbert+runs', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Hilbert+z', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Hilbert+v', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Hilbert+g', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Hilbert+r', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'Hilbert+delta', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'delta2d', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'bzip', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'gzip', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'netcdf', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix(complete, 'k2raster', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')

graphic_matrix(inner, 'Row_Major+runs', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Row_Major+z', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Row_Major+v', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Row_Major+g', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Row_Major+r', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Row_Major+delta', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Zorder+runs', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Zorder+z', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Zorder+v', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Zorder+g', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Zorder+r', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Zorder+delta', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Hilbert+runs', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Hilbert+z', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Hilbert+v', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Hilbert+g', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Hilbert+r', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'Hilbert+delta', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'delta2d', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'bzip', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'gzip', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'netcdf', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix(inner, 'k2raster', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')

graphic_matrix_2(complete, 'locality', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix_2(complete, 'Moran', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')
graphic_matrix_2(complete, 'Geary', percentajes_change, percentajes_range, x_change, x_range, output_path, 'complete')

graphic_matrix_2(inner, 'locality', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix_2(inner, 'Moran', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
graphic_matrix_2(inner, 'Geary', percentajes_change, percentajes_range, x_change, x_range, output_path, 'inner')
