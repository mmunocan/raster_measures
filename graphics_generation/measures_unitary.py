import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 3:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> <path_output>")
	sys.exit()
	
input_filename = sys.argv[1]
path_output = sys.argv[2]

df = pd.read_csv(input_filename)
df = df.sort_values(by=['locality'], ascending=False)

labels_1d = ['runs', '$r$', '$g$', '$v$', '$z$', '$\delta$', '$\delta_{2D}$']
x_1d = np.arange(len(labels_1d)-1)
x_2d = np.arange(len(labels_1d))
width = 0.9

i = 0
for index,row in df.iterrows():
	row_major_1d = []
	zorder_1d = []
	hilbert_1d = []
	random_1d = []
	
	row_major_1d.append(row['Row_Major+runs'])
	row_major_1d.append(row['Row_Major+r'])
	row_major_1d.append(row['Row_Major+g'])
	row_major_1d.append(row['Row_Major+v'])
	row_major_1d.append(row['Row_Major+z'])
	row_major_1d.append(row['Row_Major+delta'])

	zorder_1d.append(row['Zorder+runs'])
	zorder_1d.append(row['Zorder+r'])
	zorder_1d.append(row['Zorder+g'])
	zorder_1d.append(row['Zorder+v'])
	zorder_1d.append(row['Zorder+z'])
	zorder_1d.append(row['Zorder+delta'])

	hilbert_1d.append(row['Hilbert+runs'])
	hilbert_1d.append(row['Hilbert+r'])
	hilbert_1d.append(row['Hilbert+g'])
	hilbert_1d.append(row['Hilbert+v'])
	hilbert_1d.append(row['Hilbert+z'])
	hilbert_1d.append(row['Hilbert+delta'])

	random_1d.append(row['Random+runs'])
	random_1d.append(row['Random+r'])
	random_1d.append(row['Random+g'])
	random_1d.append(row['Random+v'])
	random_1d.append(row['Random+z'])
	random_1d.append(row['Random+delta'])
	
	delta_2d = np.zeros(7)
	delta_2d[6] = row['delta2d']

	row_major_1d = np.array(row_major_1d) / 1000000
	zorder_1d = np.array(zorder_1d) / 1000000
	hilbert_1d = np.array(hilbert_1d) / 1000000
	random_1d = np.array(random_1d) / 1000000
	delta_2d = delta_2d / 1000000

	plt.style.use('seaborn-poster') 
	
	fig, ax = plt.subplots(figsize=(6,5))
	ax.grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
	ax.bar(x_1d - 3*(width/8), row_major_1d, width/4, label='Row Major')
	ax.bar(x_1d - 1*(width/8), zorder_1d, width/4, label='Z-Order')
	ax.bar(x_1d + 1*(width/8), hilbert_1d, width/4, label='Hilbert')
	ax.bar(x_1d + 3*(width/8), random_1d, width/4, label='Random')
	ax.bar(x_2d, delta_2d, width, label='$\delta_{2D}$')

	ax.set_ylabel('Value (x10$^6$)')
	ax.set_xlabel('Measure')
	ax.set_xticks(x_2d)
	ax.set_xticklabels(labels_1d)
	ax.legend(loc='upper right')
	plt.title('Compressibility measures')
	fig.tight_layout()
	
	plt.savefig('{}/{}.png'.format(path_output, row['Dataset']))
	
	i = i+1