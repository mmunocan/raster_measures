import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 2:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename>")
	sys.exit()
	
input_filename = sys.argv[1]

df = pd.read_csv(input_filename)

df_original = df[df['type'] == 'original']
df_repeated = df[df['type'] == 'rep']

labels_h = ['$H_0$', '$H_1$', '$H_2$', '$H_3$', '$H_4$', '$H_5$', '$H_6$']
x_h = np.arange(len(labels_h))

labels_1d = ['runs', '$r$', '$g$', '$v$', '$z$', '$\delta$']
x_1d = np.arange(len(labels_1d))

original_h = []
repeated_h = []

original_h.append(np.mean(np.array(df_original['Row_Major+H0'])))
original_h.append(np.mean(np.array(df_original['Row_Major+H1'])))
original_h.append(np.mean(np.array(df_original['Row_Major+H2'])))
original_h.append(np.mean(np.array(df_original['Row_Major+H3'])))
original_h.append(np.mean(np.array(df_original['Row_Major+H4'])))
original_h.append(np.mean(np.array(df_original['Row_Major+H5'])))
original_h.append(np.mean(np.array(df_original['Row_Major+H6'])))

repeated_h.append(np.mean(np.array(df_repeated['Row_Major+H0'])))
repeated_h.append(np.mean(np.array(df_repeated['Row_Major+H1'])))
repeated_h.append(np.mean(np.array(df_repeated['Row_Major+H2'])))
repeated_h.append(np.mean(np.array(df_repeated['Row_Major+H3'])))
repeated_h.append(np.mean(np.array(df_repeated['Row_Major+H4'])))
repeated_h.append(np.mean(np.array(df_repeated['Row_Major+H5'])))
repeated_h.append(np.mean(np.array(df_repeated['Row_Major+H6'])))


original_1d = []
repeated_1d = []

original_1d.append(np.mean(np.array(df_original['Row_Major+runs'])))
original_1d.append(np.mean(np.array(df_original['Row_Major+r'])))
original_1d.append(np.mean(np.array(df_original['Row_Major+g'])))
original_1d.append(np.mean(np.array(df_original['Row_Major+v'])))
original_1d.append(np.mean(np.array(df_original['Row_Major+z'])))
original_1d.append(np.mean(np.array(df_original['Row_Major+delta'])))

repeated_1d.append(np.mean(np.array(df_repeated['Row_Major+runs'])))
repeated_1d.append(np.mean(np.array(df_repeated['Row_Major+r'])))
repeated_1d.append(np.mean(np.array(df_repeated['Row_Major+g'])))
repeated_1d.append(np.mean(np.array(df_repeated['Row_Major+v'])))
repeated_1d.append(np.mean(np.array(df_repeated['Row_Major+z'])))
repeated_1d.append(np.mean(np.array(df_repeated['Row_Major+delta'])))

plt.style.use('seaborn-poster') 

fig, ax = plt.subplots(figsize=(6,5))
ax.grid(linestyle = 'dotted', linewidth = 1, color = 'black')
ax.plot(x_h, original_h, label='Original', marker='o')
ax.plot(x_h, repeated_h, label='Repeated', marker='d')
ax.set_ylabel('Mean value')
ax.set_xlabel('$H_k$')
ax.set_xticks(x_h)
ax.set_xticklabels(labels_h)
ax.legend(loc='upper right')
fig.tight_layout()
plt.savefig('temporal-raster_hk.png')

width = 0.9
fig, ax = plt.subplots(figsize=(6,5))
ax.grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
ax.bar(x_1d - 1*(width/4), original_1d, width/2, label='Original')
ax.bar(x_1d + 1*(width/4), repeated_1d, width/2, label='Repeated')
ax.set_ylabel('Mean value (log scale)')
ax.set_xlabel('Measure')
ax.set_yscale('log')
ax.set_xticks(x_1d)
ax.set_xticklabels(labels_1d)
ax.legend(loc='upper right')
fig.tight_layout()
plt.savefig('temporal-raster_1d.png')
