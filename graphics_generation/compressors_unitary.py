import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 3:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> <path_output>")
	sys.exit()
	
input_filename = sys.argv[1]
path_output = sys.argv[2]

df = pd.read_csv(input_filename)

labels_2d = ['bzip', 'gzip', 'NetCDF', '$k^2$-raster']
x_2d = np.arange(len(labels_2d))
width = 0.9

i = 0
for index,row in df.iterrows():
	results = []

	results.append(row['bzip'] / (1024*1024))
	results.append(row['gzip'] / (1024*1024))
	results.append(row['netcdf'] / (1024*1024))
	results.append(row['k2raster'] / (1024*1024))
	
	plt.style.use('seaborn-poster') 
	fig, ax = plt.subplots(figsize=(6,5))
	ax.grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
	ax.bar(x_2d, results, width)
	ax.set_ylabel('Size [MB]')
	ax.set_xlabel('Compressor')
	ax.set_xticks(x_2d)
	ax.set_xticklabels(labels_2d)
	fig.tight_layout()
	
	if i < 10:
		i_name = '00{}'.format(i)
	elif i < 100:
		i_name = '0{}'.format(i)
	else:
		i_name = '{}'.format(i)
	
	plt.savefig('{}/{}.png'.format(path_output, row['Dataset']))
	
	i = i+1