#!/usr/bin/bash

mkdir -p unitary

mkdir -p unitary/eua_join1x1
python3 unitary.py csv/eua_join1X1.csv unitary/eua_join1x1
mkdir -p unitary/eua_join2x2
python3 unitary.py csv/eua_join2X2.csv unitary/eua_join2x2
mkdir -p unitary/eua_join3x3
python3 unitary.py csv/eua_join3X3.csv unitary/eua_join3x3
mkdir -p unitary/eua_join4x4
python3 unitary.py csv/eua_join4X4.csv unitary/eua_join4x4

mkdir -p unitary/cat_join1x1.0
python3 unitary.py csv/cat_join1X1.0.csv unitary/cat_join1x1.0
mkdir -p unitary/cat_join2x2.0
python3 unitary.py csv/cat_join2X2.0.csv unitary/cat_join2x2.0
mkdir -p unitary/cat_join3x3.0
python3 unitary.py csv/cat_join3X3.0.csv unitary/cat_join3x3.0
mkdir -p unitary/cat_join4x4.0
python3 unitary.py csv/cat_join4X4.0.csv unitary/cat_join4x4.0

mkdir -p unitary/cat_join1x1.3
python3 unitary.py csv/cat_join1X1.3.csv unitary/cat_join1x1.3
mkdir -p unitary/cat_join2x2.3
python3 unitary.py csv/cat_join2X2.3.csv unitary/cat_join2x2.3
mkdir -p unitary/cat_join3x3.3
python3 unitary.py csv/cat_join3X3.3.csv unitary/cat_join3x3.3
mkdir -p unitary/cat_join4x4.3
python3 unitary.py csv/cat_join4X4.3.csv unitary/cat_join4x4.3

mkdir -p entropy
python3 entropy.py csv/eua_join1X1.csv entropy/eua_join1x1.png
python3 entropy.py csv/eua_join2X2.csv entropy/eua_join2x2.png
python3 entropy.py csv/eua_join3X3.csv entropy/eua_join3x3.png
python3 entropy.py csv/eua_join4X4.csv entropy/eua_join4x4.png

python3 entropy.py csv/cat_join1X1.0.csv entropy/cat_join1x1.0.png
python3 entropy.py csv/cat_join2X2.0.csv entropy/cat_join2x2.0.png
python3 entropy.py csv/cat_join3X3.0.csv entropy/cat_join3x3.0.png
python3 entropy.py csv/cat_join4X4.0.csv entropy/cat_join4x4.0.png

python3 entropy.py csv/cat_join1X1.3.csv entropy/cat_join1x1.3.png
python3 entropy.py csv/cat_join2X2.3.csv entropy/cat_join2x2.3.png
python3 entropy.py csv/cat_join3X3.3.csv entropy/cat_join3x3.3.png
python3 entropy.py csv/cat_join4X4.3.csv entropy/cat_join4x4.3.png

mkdir -p entropy/eua_join1x1
python3 entropy_unitary.py csv/eua_join1X1.csv entropy/eua_join1x1
mkdir -p entropy/eua_join2x2
python3 entropy_unitary.py csv/eua_join2X2.csv entropy/eua_join2x2
mkdir -p entropy/eua_join3x3
python3 entropy_unitary.py csv/eua_join3X3.csv entropy/eua_join3x3
mkdir -p entropy/eua_join4x4
python3 entropy_unitary.py csv/eua_join4X4.csv entropy/eua_join4x4

mkdir -p entropy/cat_join1x1.0
python3 entropy_unitary.py csv/cat_join1X1.0.csv entropy/cat_join1x1.0
mkdir -p entropy/cat_join2x2.0
python3 entropy_unitary.py csv/cat_join2X2.0.csv entropy/cat_join2x2.0
mkdir -p entropy/cat_join3x3.0
python3 entropy_unitary.py csv/cat_join3X3.0.csv entropy/cat_join3x3.0
mkdir -p entropy/cat_join4x4.0
python3 entropy_unitary.py csv/cat_join4X4.0.csv entropy/cat_join4x4.0

mkdir -p entropy/cat_join1x1.3
python3 entropy_unitary.py csv/cat_join1X1.3.csv entropy/cat_join1x1.3
mkdir -p entropy/cat_join2x2.3
python3 entropy_unitary.py csv/cat_join2X2.3.csv entropy/cat_join2x2.3
mkdir -p entropy/cat_join3x3.3
python3 entropy_unitary.py csv/cat_join3X3.3.csv entropy/cat_join3x3.3
mkdir -p entropy/cat_join4x4.3
python3 entropy_unitary.py csv/cat_join4X4.3.csv entropy/cat_join4x4.3

mkdir -p measures
python3 measures.py csv/eua_join1X1.csv measures/eua_join1x1.png
python3 measures.py csv/eua_join2X2.csv measures/eua_join2x2.png
python3 measures.py csv/eua_join3X3.csv measures/eua_join3x3.png
python3 measures.py csv/eua_join4X4.csv measures/eua_join4x4.png

python3 measures.py csv/cat_join1X1.0.csv measures/cat_join1x1.0.png
python3 measures.py csv/cat_join2X2.0.csv measures/cat_join2x2.0.png
python3 measures.py csv/cat_join3X3.0.csv measures/cat_join3x3.0.png
python3 measures.py csv/cat_join4X4.0.csv measures/cat_join4x4.0.png

python3 measures.py csv/cat_join1X1.3.csv measures/cat_join1x1.3.png
python3 measures.py csv/cat_join2X2.3.csv measures/cat_join2x2.3.png
python3 measures.py csv/cat_join3X3.3.csv measures/cat_join3x3.3.png
python3 measures.py csv/cat_join4X4.3.csv measures/cat_join4x4.3.png

mkdir -p measures/eua_join1x1
python3 measures_unitary.py csv/eua_join1X1.csv measures/eua_join1x1
mkdir -p measures/eua_join2x2
python3 measures_unitary.py csv/eua_join2X2.csv measures/eua_join2x2
mkdir -p measures/eua_join3x3
python3 measures_unitary.py csv/eua_join3X3.csv measures/eua_join3x3
mkdir -p measures/eua_join4x4
python3 measures_unitary.py csv/eua_join4X4.csv measures/eua_join4x4

mkdir -p measures/cat_join1x1.0
python3 measures_unitary.py csv/cat_join1X1.0.csv measures/cat_join1x1.0
mkdir -p measures/cat_join2x2.0
python3 measures_unitary.py csv/cat_join2X2.0.csv measures/cat_join2x2.0
mkdir -p measures/cat_join3x3.0
python3 measures_unitary.py csv/cat_join3X3.0.csv measures/cat_join3x3.0
mkdir -p measures/cat_join4x4.0
python3 measures_unitary.py csv/cat_join4X4.0.csv measures/cat_join4x4.0

mkdir -p measures/cat_join1x1.3
python3 measures_unitary.py csv/cat_join1X1.3.csv measures/cat_join1x1.3
mkdir -p measures/cat_join2x2.3
python3 measures_unitary.py csv/cat_join2X2.3.csv measures/cat_join2x2.3
mkdir -p measures/cat_join3x3.3
python3 measures_unitary.py csv/cat_join3X3.3.csv measures/cat_join3x3.3
mkdir -p measures/cat_join4x4.3
python3 measures_unitary.py csv/cat_join4X4.3.csv measures/cat_join4x4.3

mkdir -p compressors
python3 compressors.py csv/eua_join1X1.csv compressors/eua_join1x1.png
python3 compressors.py csv/eua_join2X2.csv compressors/eua_join2x2.png
python3 compressors.py csv/eua_join3X3.csv compressors/eua_join3x3.png
python3 compressors.py csv/eua_join4X4.csv compressors/eua_join4x4.png

python3 compressors.py csv/cat_join1X1.0.csv compressors/cat_join1x1.0.png
python3 compressors.py csv/cat_join2X2.0.csv compressors/cat_join2x2.0.png
python3 compressors.py csv/cat_join3X3.0.csv compressors/cat_join3x3.0.png
python3 compressors.py csv/cat_join4X4.0.csv compressors/cat_join4x4.0.png

python3 compressors.py csv/cat_join1X1.3.csv compressors/cat_join1x1.3.png
python3 compressors.py csv/cat_join2X2.3.csv compressors/cat_join2x2.3.png
python3 compressors.py csv/cat_join3X3.3.csv compressors/cat_join3x3.3.png
python3 compressors.py csv/cat_join4X4.3.csv compressors/cat_join4x4.3.png

mkdir -p compressors/eua_join1x1
python3 compressors_unitary.py csv/eua_join1X1.csv compressors/eua_join1x1
mkdir -p compressors/eua_join2x2
python3 compressors_unitary.py csv/eua_join2X2.csv compressors/eua_join2x2
mkdir -p compressors/eua_join3x3
python3 compressors_unitary.py csv/eua_join3X3.csv compressors/eua_join3x3
mkdir -p compressors/eua_join4x4
python3 compressors_unitary.py csv/eua_join4X4.csv compressors/eua_join4x4

mkdir -p compressors/cat_join1x1.0
python3 compressors_unitary.py csv/cat_join1X1.0.csv compressors/cat_join1x1.0
mkdir -p compressors/cat_join2x2.0
python3 compressors_unitary.py csv/cat_join2X2.0.csv compressors/cat_join2x2.0
mkdir -p compressors/cat_join3x3.0
python3 compressors_unitary.py csv/cat_join3X3.0.csv compressors/cat_join3x3.0
mkdir -p compressors/cat_join4x4.0
python3 compressors_unitary.py csv/cat_join4X4.0.csv compressors/cat_join4x4.0

mkdir -p compressors/cat_join1x1.3
python3 compressors_unitary.py csv/cat_join1X1.3.csv compressors/cat_join1x1.3
mkdir -p compressors/cat_join2x2.3
python3 compressors_unitary.py csv/cat_join2X2.3.csv compressors/cat_join2x2.3
mkdir -p compressors/cat_join3x3.3
python3 compressors_unitary.py csv/cat_join3X3.3.csv compressors/cat_join3x3.3
mkdir -p compressors/cat_join4x4.3
python3 compressors_unitary.py csv/cat_join4X4.3.csv compressors/cat_join4x4.3

mkdir -p locality
python3 locality.py csv/eua_join1X1.csv locality/eua_join1x1.png
python3 locality.py csv/eua_join2X2.csv locality/eua_join2x2.png
python3 locality.py csv/eua_join3X3.csv locality/eua_join3x3.png
python3 locality.py csv/eua_join4X4.csv locality/eua_join4x4.png

python3 locality.py csv/cat_join1X1.0.csv locality/cat_join1x1.0.png
python3 locality.py csv/cat_join2X2.0.csv locality/cat_join2x2.0.png
python3 locality.py csv/cat_join3X3.0.csv locality/cat_join3x3.0.png
python3 locality.py csv/cat_join4X4.0.csv locality/cat_join4x4.0.png

python3 locality.py csv/cat_join1X1.3.csv locality/cat_join1x1.3.png
python3 locality.py csv/cat_join2X2.3.csv locality/cat_join2x2.3.png
python3 locality.py csv/cat_join3X3.3.csv locality/cat_join3x3.3.png
python3 locality.py csv/cat_join4X4.3.csv locality/cat_join4x4.3.png

mkdir -p locality/eua_join1x1
python3 locality_unitary.py csv/eua_join1X1.csv locality/eua_join1x1
mkdir -p locality/eua_join2x2
python3 locality_unitary.py csv/eua_join2X2.csv locality/eua_join2x2
mkdir -p locality/eua_join3x3
python3 locality_unitary.py csv/eua_join3X3.csv locality/eua_join3x3
mkdir -p locality/eua_join4x4
python3 locality_unitary.py csv/eua_join4X4.csv locality/eua_join4x4

mkdir -p locality/cat_join1x1.0
python3 locality_unitary.py csv/cat_join1X1.0.csv locality/cat_join1x1.0
mkdir -p locality/cat_join2x2.0
python3 locality_unitary.py csv/cat_join2X2.0.csv locality/cat_join2x2.0
mkdir -p locality/cat_join3x3.0
python3 locality_unitary.py csv/cat_join3X3.0.csv locality/cat_join3x3.0
mkdir -p locality/cat_join4x4.0
python3 locality_unitary.py csv/cat_join4X4.0.csv locality/cat_join4x4.0

mkdir -p locality/cat_join1x1.3
python3 locality_unitary.py csv/cat_join1X1.3.csv locality/cat_join1x1.3
mkdir -p locality/cat_join2x2.3
python3 locality_unitary.py csv/cat_join2X2.3.csv locality/cat_join2x2.3
mkdir -p locality/cat_join3x3.3
python3 locality_unitary.py csv/cat_join3X3.3.csv locality/cat_join3x3.3
mkdir -p locality/cat_join4x4.3
python3 locality_unitary.py csv/cat_join4X4.3.csv locality/cat_join4x4.3

mkdir -p estimations
python3 sizes.py csv/eua_join1X1_sizes.csv estimations/eua_join1X1_sizes_empirical.png estimations/eua_join1X1_sizes_theoretical.png
python3 sizes.py csv/eua_join2X2_sizes.csv estimations/eua_join2X2_sizes_empirical.png estimations/eua_join2X2_sizes_theoretical.png
python3 sizes.py csv/eua_join3X3_sizes.csv estimations/eua_join3X3_sizes_empirical.png estimations/eua_join3X3_sizes_theoretical.png
python3 sizes.py csv/eua_join4X4_sizes.csv estimations/eua_join4X4_sizes_empirical.png estimations/eua_join4X4_sizes_theoretical.png


mkdir -p  pearson
python3 pearson.py csv/eua_join1X1.csv pearson eua_join1X1
python3 pearson.py csv/eua_join2X2.csv pearson eua_join2X2
python3 pearson.py csv/eua_join3X3.csv pearson eua_join3X3
python3 pearson.py csv/eua_join4X4.csv pearson eua_join4X4

python3 pearson.py csv/cat_join1X1.0.csv pearson cat_join1X1.0
python3 pearson.py csv/cat_join2X2.0.csv pearson cat_join2X2.0
python3 pearson.py csv/cat_join3X3.0.csv pearson cat_join3X3.0
python3 pearson.py csv/cat_join4X4.0.csv pearson cat_join4X4.0

python3 pearson.py csv/cat_join1X1.1.csv pearson cat_join1X1.1
python3 pearson.py csv/cat_join1X1.2.csv pearson cat_join1X1.2

python3 pearson.py csv/cat_join1X1.3.csv pearson cat_join1X1.3
python3 pearson.py csv/cat_join2X2.3.csv pearson cat_join2X2.3
python3 pearson.py csv/cat_join3X3.3.csv pearson cat_join3X3.3
python3 pearson.py csv/cat_join4X4.3.csv pearson cat_join4X4.3

python3 sensibility.py csv/APCP_sensibility.csv sensibility
python3 temporal_raster.py csv/temporal_raster.csv
python3 delta_delta.py csv/delta_delta.csv