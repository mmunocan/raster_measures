import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 3:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> <path_output>")
	sys.exit()
	
input_filename = sys.argv[1]
path_output = sys.argv[2]

df = pd.read_csv(input_filename)

labels_2d = ["locality", "Moran's I", "Geary's C"]
x_2d = np.arange(len(labels_2d))
width = 0.9

for index,row in df.iterrows():
	results = []

	results.append(row['locality'])
	results.append(row['Moran'])
	results.append(row['Geary'])
	
	plt.style.use('seaborn-poster') 
	fig, ax = plt.subplots(figsize=(6,5))
	ax.grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
	ax.bar(x_2d, results, width)
	ax.set_ylabel('Value')
	ax.set_xlabel('Measure')
	ax.set_xticks(x_2d)
	ax.set_xticklabels(labels_2d)
	ax.text(0, results[0] / 2, '{:.2f}'.format(results[0]), ha = 'center')
	ax.text(1, results[0] / 2, '{:.5f}'.format(results[1]), ha = 'center')
	ax.text(2, results[0] / 2, '{:.5f}'.format(results[2]), ha = 'center')
	fig.tight_layout()
	plt.savefig('{}/{}.png'.format(path_output, row['Dataset']))