import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick


if len(sys.argv) != 3:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> <path_output>")
	sys.exit()
	
input_filename = sys.argv[1]
path_output = sys.argv[2]

df = pd.read_csv(input_filename)

labels_h = ['0', '1', '2', '3', '4', '5', '6']
x_h = np.arange(len(labels_h))
labels_k = ['1', '2', '3', '4', '5', '6']
x_k = np.arange(len(labels_k))

i = 0
for index,row in df.iterrows():
	row_major_h = []
	zorder_h = []
	hilbert_h = []
	random_h = []
	
	row_major_k = []
	zorder_k = []
	hilbert_k = []
	random_k = []
	
	row_major_h.append(row['Row_Major+H0'])
	row_major_h.append(row['Row_Major+H1'])
	row_major_h.append(row['Row_Major+H2'])
	row_major_h.append(row['Row_Major+H3'])
	row_major_h.append(row['Row_Major+H4'])
	row_major_h.append(row['Row_Major+H5'])
	row_major_h.append(row['Row_Major+H6'])

	zorder_h.append(row['Zorder+H0'])
	zorder_h.append(row['Zorder+H1'])
	zorder_h.append(row['Zorder+H2'])
	zorder_h.append(row['Zorder+H3'])
	zorder_h.append(row['Zorder+H4'])
	zorder_h.append(row['Zorder+H5'])
	zorder_h.append(row['Zorder+H6'])

	hilbert_h.append(row['Hilbert+H0'])
	hilbert_h.append(row['Hilbert+H1'])
	hilbert_h.append(row['Hilbert+H2'])
	hilbert_h.append(row['Hilbert+H3'])
	hilbert_h.append(row['Hilbert+H4'])
	hilbert_h.append(row['Hilbert+H5'])
	hilbert_h.append(row['Hilbert+H6'])

	random_h.append(row['Random+H0'])
	random_h.append(row['Random+H1'])
	random_h.append(row['Random+H2'])
	random_h.append(row['Random+H3'])
	random_h.append(row['Random+H4'])
	random_h.append(row['Random+H5'])
	random_h.append(row['Random+H6'])
	
	n = row['n']
	
	row_major_k.append(row['rm_k1'] / (n-1))
	row_major_k.append(row['rm_k2'] / (n-2))
	row_major_k.append(row['rm_k3'] / (n-3))
	row_major_k.append(row['rm_k4'] / (n-4))
	row_major_k.append(row['rm_k5'] / (n-5))
	row_major_k.append(row['rm_k6'] / (n-6))

	zorder_k.append(row['z_k1'] / (n-1))
	zorder_k.append(row['z_k2'] / (n-2))
	zorder_k.append(row['z_k3'] / (n-3))
	zorder_k.append(row['z_k4'] / (n-4))
	zorder_k.append(row['z_k5'] / (n-5))
	zorder_k.append(row['z_k6'] / (n-6))

	hilbert_k.append(row['h_k1'] / (n-1))
	hilbert_k.append(row['h_k2'] / (n-2))
	hilbert_k.append(row['h_k3'] / (n-3))
	hilbert_k.append(row['h_k4'] / (n-4))
	hilbert_k.append(row['h_k5'] / (n-5))
	hilbert_k.append(row['h_k6'] / (n-6))

	random_k.append(row['rd_k1'] / (n-1))
	random_k.append(row['rd_k2'] / (n-2))
	random_k.append(row['rd_k3'] / (n-3))
	random_k.append(row['rd_k4'] / (n-4))
	random_k.append(row['rd_k5'] / (n-5))
	random_k.append(row['rd_k6'] / (n-6))
	
	
	row_major_k = np.array(row_major_k) * 100
	zorder_k = np.array(zorder_k) * 100
	hilbert_k = np.array(hilbert_k) * 100
	random_k = np.array(random_k) * 100
	
	plt.style.use('seaborn-poster') 
	
	fig, ax = plt.subplots(1,2, figsize=(12,5))
	
	ax[0].grid(linestyle = 'dotted', linewidth = 1, color = 'black')
	ax[0].plot(x_h, row_major_h, label='Row Major', marker='o')
	ax[0].plot(x_h, zorder_h, label='Z-Order', marker='d')
	ax[0].plot(x_h, hilbert_h, label='Hilbert', marker='s')
	ax[0].plot(x_h, random_h, label='Random', marker='*')
	ax[0].set_ylabel('$H_k$')
	ax[0].set_xlabel('$k$')
	ax[0].set_xticks(x_h)
	ax[0].set_xticklabels(labels_h)
	ax[0].set_title('Entropy')
	ax[0].legend(loc='upper right')
	
	ax[1].grid(linestyle = 'dotted', linewidth = 1, color = 'black')
	ax[1].plot(x_k, row_major_k, label='Row Major', marker='o')
	ax[1].plot(x_k, zorder_k, label='Z-Order', marker='d')
	ax[1].plot(x_k, hilbert_k, label='Hilbert', marker='s')
	ax[1].plot(x_k, random_k, label='Random', marker='*')
	ax[1].set_ylabel('% Different / Total')
	ax[1].set_xlabel('$k$')
	ax[1].set_xticks(x_k)
	ax[1].set_xticklabels(labels_k)
	ax[1].set_title('Number of different contexts')
	ax[1].yaxis.set_major_formatter(mtick.PercentFormatter())
	
	fig.tight_layout()
	plt.savefig('{}/{}.png'.format(path_output, row['Dataset']))
	
	i = i+1