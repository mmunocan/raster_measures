import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib.pylab as pylab
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 3:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> <path_output>")
	sys.exit()
	
input_filename = sys.argv[1]
path_output = sys.argv[2]

df = pd.read_csv(input_filename)

labels_h = ['0', '1', '2', '3', '4', '5', '6']
x_h = np.arange(len(labels_h))
labels_k = ['1', '2', '3', '4', '5', '6']
x_k = np.arange(len(labels_k))
labels_1d = ['runs', '$r$', '$g$', '$v$', '$z$', '$\delta$', '$\delta_{2D}$']
x_1d = np.arange(len(labels_1d)-1)
x_2d = np.arange(len(labels_1d))
labels_compr = ['bzip', 'gzip', 'NetCDF', r'$k^2$-raster']
x_compr = np.arange(len(labels_compr))
labels_l = ['locality', "Moran's I", "Geary's C"]
x_l = np.arange(len(labels_l))

width = 0.9

plt.style.use('seaborn-poster') 

i = 0
for index,row in df.iterrows():
	row_major_h = []
	zorder_h = []
	hilbert_h = []
	random_h = []
	
	row_major_k = []
	zorder_k = []
	hilbert_k = []
	random_k = []
	n_k = []

	row_major_h.append(row['Row_Major+H0'])
	row_major_h.append(row['Row_Major+H1'])
	row_major_h.append(row['Row_Major+H2'])
	row_major_h.append(row['Row_Major+H3'])
	row_major_h.append(row['Row_Major+H4'])
	row_major_h.append(row['Row_Major+H5'])
	row_major_h.append(row['Row_Major+H6'])

	zorder_h.append(row['Zorder+H0'])
	zorder_h.append(row['Zorder+H1'])
	zorder_h.append(row['Zorder+H2'])
	zorder_h.append(row['Zorder+H3'])
	zorder_h.append(row['Zorder+H4'])
	zorder_h.append(row['Zorder+H5'])
	zorder_h.append(row['Zorder+H6'])

	hilbert_h.append(row['Hilbert+H0'])
	hilbert_h.append(row['Hilbert+H1'])
	hilbert_h.append(row['Hilbert+H2'])
	hilbert_h.append(row['Hilbert+H3'])
	hilbert_h.append(row['Hilbert+H4'])
	hilbert_h.append(row['Hilbert+H5'])
	hilbert_h.append(row['Hilbert+H6'])

	random_h.append(row['Random+H0'])
	random_h.append(row['Random+H1'])
	random_h.append(row['Random+H2'])
	random_h.append(row['Random+H3'])
	random_h.append(row['Random+H4'])
	random_h.append(row['Random+H5'])
	random_h.append(row['Random+H6'])
	
	n = row['n']
	
	row_major_k.append(row['rm_k1'] / (n-1))
	row_major_k.append(row['rm_k2'] / (n-2))
	row_major_k.append(row['rm_k3'] / (n-3))
	row_major_k.append(row['rm_k4'] / (n-4))
	row_major_k.append(row['rm_k5'] / (n-5))
	row_major_k.append(row['rm_k6'] / (n-6))

	zorder_k.append(row['z_k1'] / (n-1))
	zorder_k.append(row['z_k2'] / (n-2))
	zorder_k.append(row['z_k3'] / (n-3))
	zorder_k.append(row['z_k4'] / (n-4))
	zorder_k.append(row['z_k5'] / (n-5))
	zorder_k.append(row['z_k6'] / (n-6))

	hilbert_k.append(row['h_k1'] / (n-1))
	hilbert_k.append(row['h_k2'] / (n-2))
	hilbert_k.append(row['h_k3'] / (n-3))
	hilbert_k.append(row['h_k4'] / (n-4))
	hilbert_k.append(row['h_k5'] / (n-5))
	hilbert_k.append(row['h_k6'] / (n-6))

	random_k.append(row['rd_k1'] / (n-1))
	random_k.append(row['rd_k2'] / (n-2))
	random_k.append(row['rd_k3'] / (n-3))
	random_k.append(row['rd_k4'] / (n-4))
	random_k.append(row['rd_k5'] / (n-5))
	random_k.append(row['rd_k6'] / (n-6))
	
	
	row_major_k = np.array(row_major_k) * 100
	zorder_k = np.array(zorder_k) * 100
	hilbert_k = np.array(hilbert_k) * 100
	random_k = np.array(random_k) * 100
	
	row_major_1d = []
	zorder_1d = []
	hilbert_1d = []
	random_1d = []
	
	row_major_1d.append(row['Row_Major+runs'])
	row_major_1d.append(row['Row_Major+r'])
	row_major_1d.append(row['Row_Major+g'])
	row_major_1d.append(row['Row_Major+v'])
	row_major_1d.append(row['Row_Major+z'])
	row_major_1d.append(row['Row_Major+delta'])

	zorder_1d.append(row['Zorder+runs'])
	zorder_1d.append(row['Zorder+r'])
	zorder_1d.append(row['Zorder+g'])
	zorder_1d.append(row['Zorder+v'])
	zorder_1d.append(row['Zorder+z'])
	zorder_1d.append(row['Zorder+delta'])

	hilbert_1d.append(row['Hilbert+runs'])
	hilbert_1d.append(row['Hilbert+r'])
	hilbert_1d.append(row['Hilbert+g'])
	hilbert_1d.append(row['Hilbert+v'])
	hilbert_1d.append(row['Hilbert+z'])
	hilbert_1d.append(row['Hilbert+delta'])

	random_1d.append(row['Random+runs'])
	random_1d.append(row['Random+r'])
	random_1d.append(row['Random+g'])
	random_1d.append(row['Random+v'])
	random_1d.append(row['Random+z'])
	random_1d.append(row['Random+delta'])
	
	delta_2d = np.zeros(7)
	delta_2d[6] = row['delta2d']

	row_major_1d = np.array(row_major_1d) / 1000000
	zorder_1d = np.array(zorder_1d) / 1000000
	hilbert_1d = np.array(hilbert_1d) / 1000000
	random_1d = np.array(random_1d) / 1000000
	delta_2d = delta_2d / 1000000
	
	results = []

	results.append(row['bzip'] / (1024*1024))
	results.append(row['gzip'] / (1024*1024))
	results.append(row['netcdf'] / (1024*1024))
	results.append(row['k2raster'] / (1024*1024))
	
	results_l = []

	results_l.append(row['locality'])
	results_l.append(row['Moran'])
	results_l.append(row['Geary'])
	
	fig, ax = plt.subplots(3,2, figsize=(12, 15))
	
	ax[0][0].grid(linestyle = 'dotted', linewidth = 1, color = 'black')
	ax[0][0].plot(x_h, row_major_h, label='Row Major', marker='o')
	ax[0][0].plot(x_h, zorder_h, label='Z-Order', marker='d')
	ax[0][0].plot(x_h, hilbert_h, label='Hilbert', marker='s')
	ax[0][0].plot(x_h, random_h, label='Random', marker='*')
	ax[0][0].set_ylabel('$H_k$')
	ax[0][0].set_xlabel('$k$')
	ax[0][0].set_xticks(x_h)
	ax[0][0].set_xticklabels(labels_h)
	ax[0][0].set_title('Entropy')
	ax[0][0].legend(loc='upper right')
	
	ax[0][1].grid(linestyle = 'dotted', linewidth = 1, color = 'black')
	ax[0][1].plot(x_k, row_major_k, label='Row Major', marker='o')
	ax[0][1].plot(x_k, zorder_k, label='Z-Order', marker='d')
	ax[0][1].plot(x_k, hilbert_k, label='Hilbert', marker='s')
	ax[0][1].plot(x_k, random_k, label='Random', marker='*')
	ax[0][1].set_ylabel('% Different / Total')
	ax[0][1].set_xlabel('$k$')
	ax[0][1].set_xticks(x_k)
	ax[0][1].set_xticklabels(labels_k)
	ax[0][1].set_title('Different contexts')
	ax[0][1].yaxis.set_major_formatter(mtick.PercentFormatter())
	ax[0][1].legend(loc='upper right')
	
	ax[1][0].grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
	ax[1][0].bar(x_1d - 3*(width/8), row_major_1d, width/4, label='Row Major')
	ax[1][0].bar(x_1d - 1*(width/8), zorder_1d, width/4, label='Z-Order')
	ax[1][0].bar(x_1d + 1*(width/8), hilbert_1d, width/4, label='Hilbert')
	ax[1][0].bar(x_1d + 3*(width/8), random_1d, width/4, label='Random')
	ax[1][0].bar(x_2d, delta_2d, width, label=r'$\delta_{2D}$')
	ax[1][0].set_ylabel('Value (x10$^6$)')
	ax[1][0].set_xlabel('Measure')
	ax[1][0].set_xticks(x_2d)
	ax[1][0].set_xticklabels(labels_1d)
	ax[1][0].set_title('Compressibility measures')
	ax[1][0].legend(loc='upper right')
	
	ax[1][1].grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
	ax[1][1].bar(x_compr, results, width, linewidth=1)
	ax[1][1].set_ylabel('Size [MB]')
	ax[1][1].set_xlabel('Compressor')
	ax[1][1].set_xticks(x_compr)
	ax[1][1].set_xticklabels(labels_compr)
	ax[1][1].set_title('Compression size')
	
	ax[2][0].grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
	ax[2][0].bar(x_l, results_l, width, linewidth=1)
	ax[2][0].set_ylabel('Value')
	ax[2][0].set_xlabel('Measure')
	ax[2][0].set_xticks(x_l)
	ax[2][0].set_xticklabels(labels_l)
	ax[2][0].text(0, results_l[0] / 2, '{:.2f}'.format(results_l[0]), ha = 'center')
	ax[2][0].text(1, results_l[0] / 2, '{:.5f}'.format(results_l[1]), ha = 'center')
	ax[2][0].text(2, results_l[0] / 2, '{:.5f}'.format(results_l[2]), ha = 'center')
	ax[2][0].set_title('Spatial Autocorrelation and Locality')
	
	ax[2][1].set_visible(False)
	
	fig.tight_layout()
	plt.savefig('{}/{}.png'.format(path_output, row['Dataset']))
	
	i = i+1