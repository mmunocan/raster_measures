import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 2:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> ")
	sys.exit()
	
input_filename = sys.argv[1]

df = pd.read_csv(input_filename)

delta_2d = np.array(df['delta_2d']) / 1000000
delta_delta = np.array(df['delta_delta']) / 1000000

bzip = np.array(df['bzip']) / (1024*1024)
gzip = np.array(df['gzip']) / (1024*1024)
netcdf = np.array(df['netcdf']) / (1024*1024)
k2raster = np.array(df['k2raster']) / (1024*1024)

x_h = np.arange(len(delta_2d)) * 1000

plt.style.use('seaborn-poster') 

fig, ax = plt.subplots(figsize=(6,5))
ax.grid(linestyle = 'dotted', linewidth = 1, color = 'black')
ax.plot(x_h, delta_2d, label='$\delta_{2D}$', marker='o')
ax.plot(x_h, delta_delta, label='$\delta_{\Delta}$', marker='d')
ax.set_ylabel('Value (x10$^6$)')
ax.set_xlabel('$a$')
ax.set_xticks(x_h)
ax.set_xticklabels(x_h, rotation=45)
ax.legend(loc='upper left')
fig.tight_layout()
plt.savefig('delta_delta-comp.png')

fig, ax = plt.subplots(figsize=(6,5))
ax.grid(linestyle = 'dotted', linewidth = 1, color = 'black')
ax.plot(x_h, bzip, label='bzip', marker='o')
ax.plot(x_h, gzip, label='gzip', marker='d')
ax.plot(x_h, netcdf, label='NetCDF', marker='s')
ax.plot(x_h, k2raster, label='$k^2$-raster', marker='*')
ax.set_ylabel('Size [MB]')
ax.set_xlabel('$a$')
ax.set_xticks(x_h)
ax.set_xticklabels(x_h, rotation=45)
ax.legend(loc='lower right')
fig.tight_layout()
plt.savefig('delta_delta-size.png')
