import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import style
import matplotlib.ticker as mtick

if len(sys.argv) != 3:
	print("ERROR! USE python3 " + sys.argv[0] + " <input_filename> <output_filename>")
	sys.exit()
	
input_filename = sys.argv[1]
output_filename = sys.argv[2]

df = pd.read_csv(input_filename)

labels_2d = ['bzip', 'gzip', 'NetCDF', '$k^2$-raster']
x_2d = np.arange(len(labels_2d))
width = 0.9

results = []

results.append(np.mean(np.array(df['bzip'])) / (1024*1024))
results.append(np.mean(np.array(df['gzip'])) / (1024*1024))
results.append(np.mean(np.array(df['netcdf'])) / (1024*1024))
results.append(np.mean(np.array(df['k2raster'])) / (1024*1024))

plt.style.use('seaborn-poster') 
fig, ax = plt.subplots(figsize=(6,5))
ax.grid(axis = 'y', linestyle = 'dotted', linewidth = 1, color = 'black')
ax.bar(x_2d, results, width)
ax.set_ylabel('Mean size [MB]')
ax.set_xlabel('Compressor')
ax.set_xticks(x_2d)
ax.set_xticklabels(labels_2d)
fig.tight_layout()
plt.savefig(output_filename)