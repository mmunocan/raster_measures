/* 
 * Given a int raster, compute \delta_{2D} measure
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <map>
#include <utility>
#include <climits>
#include <cmath>
#include <chrono>

using namespace std;

void read_dataset(const string & filename, vector<vector<int>> & raster){
	
	size_t n_rows = raster.size();
	size_t n_cols = raster[0].size();
	
	ifstream file;
	file.open(filename, ios::binary);
	assert(file.is_open() && file.good());
	
	int value;
	for(size_t i = 0; i < n_rows; i++){
		for(size_t j = 0; j < n_cols; j++){
			file.read((char *)(& value), sizeof(int));
			raster[i][j] = value;
		}
	}
	file.close();
	
}

/*
 * Given an squared submatrix [i..i+k-1]x[j..j+k-1], generates the string equivalent
 * that covers the submatrix per each byte in row-major
 */
string get_submatrix(vector<vector<int>> & raster, size_t i, size_t j, size_t k){
	
	size_t n = k * k;
	int * submatrix = new int[n];
	
	for(size_t l = 0; l < k; l++){
		for(size_t m = 0; m < k; m++){
			submatrix[(l*k)+m] = raster[i+l][j+m];
		}
	}
	
	char * submatrix_chr = (char*) submatrix;
	string submatrix_str;
	submatrix_str.assign(submatrix_chr, submatrix_chr + n*sizeof(int));
	return submatrix_str;
	
}

size_t computing_s_k(vector<vector<int>> & raster, size_t k){
	
	size_t n_rows = raster.size();
	size_t n_cols = raster[0].size();
	map<string, size_t> frequencies;
	int s_k = 0;
	
	for(size_t i = 0; i < (n_rows - k + 1); i++){
		for(size_t j = 0; j < (n_cols - k + 1); j++){
			string submatrix = get_submatrix(raster, i, j, k);
			if(!frequencies.count(submatrix)){
				frequencies[submatrix] = 1;
				s_k++;
			}
		}
	}
	
	return s_k;
	
}

double computing_d_k(vector<vector<int>> & raster, size_t k){
	return (double)((double)computing_s_k(raster, k) / (k*k));
}

pair<double,size_t> compute_delta_2d_measure(const string & filename, size_t n_rows, size_t n_cols){
	vector<vector<int>> raster(n_rows, vector<int>(n_cols));
	read_dataset(filename, raster);
	
	size_t min_val = min(n_rows, n_cols);
	
	vector<size_t> d_k(min_val+1);
	d_k[0] = 0;
	
	for(size_t k = 1; k <= min_val; k++){
		d_k[k] = computing_d_k(raster, k);
		if(k != 1 && d_k[k-1] > d_k[k]){
			return make_pair(d_k[k-1], k-1);
		}
	}
	
	exit(-1);	// An error has ocurred
}

int main(int argc, char ** argv){
	
	if(argc != 4){
		cout << "ERROR! USE " << argv[0] << " <filename> <n_rows> <n_cols> " << endl;
		return -1;
	}
	
	string filename = argv[1];
	size_t n_rows = atoi(argv[2]);
	size_t n_cols = atoi(argv[3]);
	
	auto start = chrono::high_resolution_clock::now();
	pair<double,size_t> delta = compute_delta_2d_measure(filename, n_rows, n_cols);
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::seconds> (finish - start).count();
	
	cout << filename << ";" << delta.first << ";" << delta.second << ";" << time << endl;
	
	return 0;
}