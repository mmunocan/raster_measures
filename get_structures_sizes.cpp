/*
 * Given the raster compressed with bzip, gzip and NetCDF, print the compressed files size
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <string>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 2){
		cerr << "ERROR! USE " << argv[0] << " <ifilename> " << endl;
		return -1;
	}
	
	string filename = argv[1];
	string filename_bz = filename + ".bz2";
	string filename_gz = filename + ".gz";
	string filename_nc = filename + ".nc";
	
	ifstream file_bz;
	file_bz.open(filename_bz, ios::binary);
	assert(file_bz.is_open() && file_bz.good());
	file_bz.seekg(0, ios::end);
	size_t bz_size = file_bz.tellg();
	file_bz.seekg(0, ios::beg);
	file_bz.close();
	
	ifstream file_gz;
	file_gz.open(filename_gz, ios::binary);
	assert(file_gz.is_open() && file_gz.good());
	file_gz.seekg(0, ios::end);
	size_t gz_size = file_gz.tellg();
	file_gz.seekg(0, ios::beg);
	file_gz.close();
	
	ifstream file_nc;
	file_nc.open(filename_nc, ios::binary);
	assert(file_nc.is_open() && file_nc.good());
	file_nc.seekg(0, ios::end);
	size_t nc_size = file_nc.tellg();
	file_nc.seekg(0, ios::beg);
	file_nc.close();
	
	cout << filename << ";" << bz_size << ";" << gz_size << ";" << nc_size << endl;
	
}