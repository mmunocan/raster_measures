/* 
 * Given a int raster, generates an output raster compressed by NetCDF in level 9
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <netcdf.h>

using namespace std;

int main(int argc, char ** argv) {
	
	if(argc != 4){
		cerr << "ERROR! USE " << argv[0] << " <filename> <n_rows> <n_cols> " << endl;
		return -1;
	}
	
	string filename = argv[1];
	size_t n_rows = atoi(argv[2]);	// height
	size_t n_cols = atoi(argv[3]);	// width
	
	string net_filename = filename + ".nc";
	
    int * data = new int[n_cols*n_rows];

    ifstream file;
	file.open(filename, ios::binary);
	assert(file.is_open() && file.good());
	
	file.read((char*)data, n_rows * n_cols * sizeof(int));
	file.close();
	
    // Create a NetCDF file for writing
    int ncid;
    if (nc_create(net_filename.c_str(), NC_CLOBBER | NC_NETCDF4, &ncid) != NC_NOERR) {
		std::cerr << "Error creating NetCDF file." << std::endl;
        return 1;
    }
	
	int xDimID, yDimID;
    nc_def_dim(ncid, "x", n_cols, &xDimID);
    nc_def_dim(ncid, "y", n_rows, &yDimID);
	
    int varID;
	int dimIDs[2] = {xDimID, yDimID};
    nc_def_var(ncid, "raster", NC_INT, 2, &dimIDs[0], &varID);
	
	nc_def_var_deflate(ncid, varID, 1, 1, 9);
    
	nc_enddef(ncid);
    nc_put_var_int(ncid, varID, data);
    nc_close(ncid);
	
    return 0;
}
