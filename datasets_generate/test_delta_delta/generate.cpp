#include <iostream>
#include <fstream>
#include <cassert>
#include <random>
#include <string>
#include <map>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <cstdlib>

using namespace std;

int main(int argc, char ** argv){
	
	int n = 512;
	int s = 1;
	int e = 500;
	int end_test_v1 = 1000;
	int salt = 100;
	
	int* raster_base = new int[n*n];
	
	random_device dev;
	mt19937 rng(dev());
	uniform_int_distribution<mt19937::result_type> dist(s, e);
	
	for(int i = 0; i < n*n; i++){
		raster_base[i] = dist(rng);
	}
	
	int* raster = new int[n*n];
	
	string filename = "v1_";
	ofstream output_guide;
	output_guide.open("v1_test.txt");
	ofstream output_file;
	string current_filename;
	
	int a = 0;
	
	while(a <= end_test_v1){
		for(int i = 0; i < n*n; i++){
			raster[i] = raster_base[i] * (a+1);
		}
		
		current_filename = filename+to_string(a)+".bin";
		output_guide << current_filename << " " << n << " " << n << endl;
		output_file.open(current_filename, ios::binary);
		assert(output_file.is_open() && output_file.good());
		output_file.write((char *)(raster), sizeof(int) * n*n);
		output_file.close();
		
		a += salt;
	}
	
	output_guide.close();
	
	
	
	srand(unsigned(time(0)));
	
	for(int i = 0; i < n*n; i++){
		if(i < (n*n)/2){
			raster_base[i] = 1;
		}else if(i < (3*n*n)/4){
			raster_base[i] = 2;
		}else if(i < (7*n*n)/8){
			raster_base[i] = 3;
		}else if(i < (15*n*n)/16){
			raster_base[i] = 4;
		}else{
			raster_base[i] = 5;
		}
	}
	
	random_shuffle(raster_base, raster_base+(n*n));
	
	int* values = new int[5];
	for(int i = 0; i < 5; i++){
		values[i] = (i+1)*100;
	}
	int mean = (values[0]*n*n/2) + (values[1]*n*n/4) + (values[2]*n*n/8) + (values[3]*n*n/16) + (values[4]*n*n/16);
	map<int, int> mp;
	
	filename = "v2_";
	output_guide.open("v2_test.txt");
	
	ofstream shuffle_info;
	shuffle_info.open("shuffle_info.txt");
	
	a = 0;
	int raster_mean, I;
	while(a < 100){
		random_shuffle(values, values+5);
		
		raster_mean = (values[0]*n*n/2) + (values[1]*n*n/4) + (values[2]*n*n/8) + (values[3]*n*n/16) + (values[4]*n*n/16);
		I = abs(mean-raster_mean);
		
		shuffle_info << I << ";[";
		for(int i = 0; i < 5; i++){
			shuffle_info << values[i];
			if(i != 4){
				shuffle_info << ", ";
			}
		}
		shuffle_info << "]" << endl;
		
		for(int i = 0; i < 5; i++){
			mp[i+1] = values[i];
		}
		
		for(int i = 0; i < n*n; i++){
			raster[i] = mp[raster_base[i]];
		}
		
		current_filename = filename+to_string(a)+".bin";
		output_guide << current_filename << " " << n << " " << n << endl;
		output_file.open(current_filename, ios::binary);
		assert(output_file.is_open() && output_file.good());
		output_file.write((char *)(raster), sizeof(int) * n*n);
		output_file.close();
		
		a++;
	}
	shuffle_info.close();
	
	output_guide.close();
	
	delete[] raster;
	delete[] raster_base;
	
	return 0;
}