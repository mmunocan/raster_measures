#!/usr/bin/bash

./generate_pattern 224 464 0.05 apcp/chg_0005.bin
./generate_pattern 224 464 0.1  apcp/chg_001.bin
./generate_pattern 224 464 1    apcp/chg_01.bin
./generate_pattern 224 464 2    apcp/chg_02.bin
./generate_pattern 224 464 5    apcp/chg_05.bin
./generate_pattern 224 464 10   apcp/chg_10.bin
./generate_pattern 224 464 20   apcp/chg_20.bin
./generate_pattern 224 464 50   apcp/chg_50.bin

./generate_pattern_inner apcp/APCP.bin 224 464 0.05 apcp/chg_in_0005.bin
./generate_pattern_inner apcp/APCP.bin 224 464 0.1  apcp/chg_in_001.bin
./generate_pattern_inner apcp/APCP.bin 224 464 1    apcp/chg_in_01.bin
./generate_pattern_inner apcp/APCP.bin 224 464 2    apcp/chg_in_02.bin
./generate_pattern_inner apcp/APCP.bin 224 464 5    apcp/chg_in_05.bin
./generate_pattern_inner apcp/APCP.bin 224 464 10   apcp/chg_in_10.bin
./generate_pattern_inner apcp/APCP.bin 224 464 20   apcp/chg_in_20.bin
./generate_pattern_inner apcp/APCP.bin 224 464 50   apcp/chg_in_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 1 apcp/APCP_0005_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 2 apcp/APCP_0005_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 5 apcp/APCP_0005_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 10 apcp/APCP_0005_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 20 apcp/APCP_0005_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 30 apcp/APCP_0005_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 40 apcp/APCP_0005_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_0005.bin 50 apcp/APCP_0005_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 1 apcp/APCP_001_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 2 apcp/APCP_001_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 5 apcp/APCP_001_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 10 apcp/APCP_001_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 20 apcp/APCP_001_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 30 apcp/APCP_001_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 40 apcp/APCP_001_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_001.bin 50 apcp/APCP_001_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 1 apcp/APCP_01_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 2 apcp/APCP_01_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 5 apcp/APCP_01_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 10 apcp/APCP_01_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 20 apcp/APCP_01_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 30 apcp/APCP_01_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 40 apcp/APCP_01_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_01.bin 50 apcp/APCP_01_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 1 apcp/APCP_02_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 2 apcp/APCP_02_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 5 apcp/APCP_02_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 10 apcp/APCP_02_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 20 apcp/APCP_02_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 30 apcp/APCP_02_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 40 apcp/APCP_02_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_02.bin 50 apcp/APCP_02_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 1 apcp/APCP_05_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 2 apcp/APCP_05_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 5 apcp/APCP_05_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 10 apcp/APCP_05_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 20 apcp/APCP_05_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 30 apcp/APCP_05_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 40 apcp/APCP_05_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_05.bin 50 apcp/APCP_05_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 1 apcp/APCP_10_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 2 apcp/APCP_10_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 5 apcp/APCP_10_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 10 apcp/APCP_10_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 20 apcp/APCP_10_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 30 apcp/APCP_10_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 40 apcp/APCP_10_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_10.bin 50 apcp/APCP_10_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 1 apcp/APCP_20_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 2 apcp/APCP_20_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 5 apcp/APCP_20_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 10 apcp/APCP_20_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 20 apcp/APCP_20_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 30 apcp/APCP_20_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 40 apcp/APCP_20_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_20.bin 50 apcp/APCP_20_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 1 apcp/APCP_50_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 2 apcp/APCP_50_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 5 apcp/APCP_50_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 10 apcp/APCP_50_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 20 apcp/APCP_50_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 30 apcp/APCP_50_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 40 apcp/APCP_50_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_50.bin 50 apcp/APCP_50_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 1 apcp/APCP_in_0005_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 2 apcp/APCP_in_0005_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 5 apcp/APCP_in_0005_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 10 apcp/APCP_in_0005_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 20 apcp/APCP_in_0005_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 30 apcp/APCP_in_0005_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 40 apcp/APCP_in_0005_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_0005.bin 50 apcp/APCP_in_0005_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 1 apcp/APCP_in_001_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 2 apcp/APCP_in_001_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 5 apcp/APCP_in_001_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 10 apcp/APCP_in_001_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 20 apcp/APCP_in_001_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 30 apcp/APCP_in_001_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 40 apcp/APCP_in_001_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_001.bin 50 apcp/APCP_in_001_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 1 apcp/APCP_in_01_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 2 apcp/APCP_in_01_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 5 apcp/APCP_in_01_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 10 apcp/APCP_in_01_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 20 apcp/APCP_in_01_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 30 apcp/APCP_in_01_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 40 apcp/APCP_in_01_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_01.bin 50 apcp/APCP_in_01_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 1 apcp/APCP_in_02_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 2 apcp/APCP_in_02_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 5 apcp/APCP_in_02_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 10 apcp/APCP_in_02_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 20 apcp/APCP_in_02_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 30 apcp/APCP_in_02_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 40 apcp/APCP_in_02_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_02.bin 50 apcp/APCP_in_02_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 1 apcp/APCP_in_05_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 2 apcp/APCP_in_05_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 5 apcp/APCP_in_05_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 10 apcp/APCP_in_05_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 20 apcp/APCP_in_05_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 30 apcp/APCP_in_05_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 40 apcp/APCP_in_05_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_05.bin 50 apcp/APCP_in_05_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 1 apcp/APCP_in_10_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 2 apcp/APCP_in_10_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 5 apcp/APCP_in_10_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 10 apcp/APCP_in_10_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 20 apcp/APCP_in_10_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 30 apcp/APCP_in_10_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 40 apcp/APCP_in_10_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_10.bin 50 apcp/APCP_in_10_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 1 apcp/APCP_in_20_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 2 apcp/APCP_in_20_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 5 apcp/APCP_in_20_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 10 apcp/APCP_in_20_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 20 apcp/APCP_in_20_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 30 apcp/APCP_in_20_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 40 apcp/APCP_in_20_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_20.bin 50 apcp/APCP_in_20_50.bin

./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 1 apcp/APCP_in_50_01.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 2 apcp/APCP_in_50_02.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 5 apcp/APCP_in_50_05.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 10 apcp/APCP_in_50_10.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 20 apcp/APCP_in_50_20.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 30 apcp/APCP_in_50_30.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 40 apcp/APCP_in_50_40.bin
./generate_sensibility apcp/APCP.bin apcp/chg_in_50.bin 50 apcp/APCP_in_50_50.bin