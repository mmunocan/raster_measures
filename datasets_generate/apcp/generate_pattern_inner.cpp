#include <iostream>
#include <fstream>
#include <cassert>
#include <climits>
#include <vector>
#include <algorithm>
#include <random>
#include <cmath>

using namespace std;

int main(int argc, char ** argv){
	
	if(argc != 6){
		cerr << "ERROR! USE " << argv[0] << " <filename> <n_rows> <n_cols> <percentaje> <pattern_filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	int n_rows = atoi(argv[2]);
	int n_cols = atoi(argv[3]);
	float percentaje = atof(argv[4]);
	string pattern_filename = argv[5];
	
	int n = n_rows * n_cols;
	
	// Reading the input
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	int * data = new int[n];
	input_file.read((char*)data, n * sizeof(int));
	input_file.close();
	
	// Generate and shuffle cells id
	int no_data_value = -1;
	vector<int> cells_id;
	for(int i = 0; i < n; i++){
		if(data[i] != no_data_value){
			cells_id.push_back(i);
		}
	}
	
	auto rng = std::default_random_engine {};
	shuffle(begin(cells_id), end(cells_id), rng);
	
	// Select cells to change
	int to_change = (int)(((double)percentaje / (double)100) * (double)n);
	to_change = min(to_change, (int)cells_id.size());
	
	vector<int> changed(n, 0);
	for(int i = 0; i < to_change; i++){
		int r = (int)floor(cells_id[i] / n_cols);
		int c = (int)floor(cells_id[i] % n_cols);
		changed[cells_id[i]] = 1;
	}
	
	
	// Write output
	ofstream output_file;
	output_file.open(pattern_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)changed.data(), n * sizeof(int));
	output_file.close();
	
	cout << "File " << pattern_filename << " is ready!" << endl;
	
	return 0;
}