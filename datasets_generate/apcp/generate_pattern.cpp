#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <algorithm>
#include <random>
#include <cmath>

using namespace std;

int main(int argc, char ** argv){
	
	if(argc != 5){
		cerr << "ERROR! USE " << argv[0] << " <n_rows> <n_cols> <percentaje> <pattern_filename>" << endl;
		return -1;
	}
	
	int n_rows = atoi(argv[1]);
	int n_cols = atoi(argv[2]);
	float percentaje = atof(argv[3]);
	string pattern_filename = argv[4];
	
	int n = n_rows * n_cols;
	
	// Generate and shuffle cells id
	vector<int> cells_id(n);
	for(int i = 0; i < n; i++){
		cells_id[i] = i;
	}
	
	auto rng = std::default_random_engine {};
	shuffle(begin(cells_id), end(cells_id), rng);
	
	// Select cells to change
	int to_change = (int)(((double)percentaje / (double)100) * (double)n);
	
	vector<int> changed(n, 0);
	for(int i = 0; i < to_change; i++){
		int r = (int)floor(cells_id[i] / n_cols);
		int c = (int)floor(cells_id[i] % n_cols);
		changed[cells_id[i]] = 1;
	}
	
	// Write output
	ofstream output_file;
	output_file.open(pattern_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)changed.data(), n * sizeof(int));
	output_file.close();
	
	cout << "File " << pattern_filename << " is ready!" << endl;
	
	return 0;
}