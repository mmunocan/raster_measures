#include <iostream>
#include <fstream>
#include <cassert>
#include <climits>
#include <vector>
#include <random>

using namespace std;

int main(int argc, char ** argv){
	
	if(argc != 5){
		cerr << "ERROR! USE " << argv[0] << " <filename> <pattern_filename> <percentaje> <output_filename>" << endl;
		return -1;
	}
	
	string filename = argv[1];
	string pattern_filename = argv[2];
	float percentaje = atof(argv[3]);
	string output_filename = argv[4];
	
	// Reading the input
	ifstream input_file;
	input_file.open(filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	int * data = new int[n];
	input_file.read((char*)data, n * sizeof(int));
	input_file.close();
	
	// Reading de input_pattern
	ifstream pattern_file;
	pattern_file.open(pattern_filename, ios::binary);
	assert(pattern_file.is_open() && pattern_file.good());
	int * pattern = new int[n];
	pattern_file.read((char*)pattern, n * sizeof(int));
	pattern_file.close();
	
	// Get min, max and number of rasters to change
	int min = INT_MAX;
	int max = INT_MIN;
	
	for(size_t i = 0; i < n; i++){
		if(data[i] < min)	min = data[i];
		if(data[i] > max)	max = data[i];
	}
	
	int range = (int)(((double)percentaje / (double)100) * (double)(max-min));
	
	// Change 'to_change' cells
	random_device dev;
    mt19937 rng(dev());
	uniform_int_distribution<mt19937::result_type> dist_value(1, range);
	int random_value = 0;
	for(int i = 0; i < n; i++){
		if(pattern[i] == 1){
			random_value = dist_value(rng);
			data[i] = data[i] + random_value;
		}
	}
	
	// Write output
	ofstream output_file;
	output_file.open(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)data, n * sizeof(int));
	output_file.close();
	
	cout << "File " << output_filename << " is ready!" << endl;
	
	return 0;
}